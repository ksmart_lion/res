<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('using', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('warehouse_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('note')->nullable();
            $table->date('using_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('using');
    }
}
