<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('table_id')->unsigned()->nullable();
            $table->double('totalitems')->nullable();
            $table->double('discountamount')->nullable();
            $table->double('taxamount')->nullable();
            $table->double('tax')->nullable();
            $table->double('paid')->nullable();
            $table->double('total')->nullable();
            $table->double('discount')->nullable();
            $table->double('change')->nullable();
            $table->string('created_by')->nullable();
            $table->boolean('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
