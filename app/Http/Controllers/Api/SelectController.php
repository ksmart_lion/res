<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use App\Models\SaleItem;
use App\Models\Sall;
use App\Models\Temporary;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PhpParser\Node\Expr\New_;

class SelectController extends Controller
{
    //

    public function select(Request $request){
       //dd($request->all());
        $product_id =$request->product_id;
        $table_id =$request->number;
        $price =$request->price;
        $m = Temporary::where('product_id',$product_id)->where('table_id',$table_id)
            ->whereDate('created_at',date('Y-m-d'))->first();

        if($m == null){
            $m = new Temporary();
        }

        if($m != null) {
            $qty = $m->qty >0?$m->qty:0;
            $m->table_id = $table_id;
            $m->product_id = $product_id;
            $m->price = $price;
            $m->qty = $qty + 1;
            $m->save();
        }
         return response()->json(['status'=>true]);
    }


    public function posale(Request $request){
         //$product_id=$request->
//        $getsale=Temporary::join('products','id','=','product_id')
//                          ->select('products.name','products.price')->get();

        return view('order.pos.load_posales');
    }

    public function update_qty(Request $request,$id){
     $update_tem=Temporary::where('id',$id)->first();
     $qty=$request->qt;
     $update_tem->qty=$qty;
     $update_tem->update();
    }

    public function  remove_item($id){
        //dd($id);
      Temporary::where('id',$id)->delete();
    }

    public function  total_item(){
        $item = Temporary::where('table_id',session('table_id'))
            ->whereDate('created_at',date('Y-m-d'))->sum('qty');
        return $item;
    }
    public function  subtotal(){
        $totals = Temporary::where('table_id',session('table_id'))
            ->whereDate('created_at',date('Y-m-d'))->get();
        $subtotal=0;

        foreach ($totals as $total){
            $subtotal+=$total->qty*$total->price;
            //dd($total->qty);
        }

        return $subtotal;
       //dd("this is subtotal ="+$subtotal);
    }

    public function reset_pos(){
        Temporary::where('table_id',session('table_id'))
            ->whereDate('created_at',date('Y-m-d'))->delete();

    }

//    public function get_discount(Request $request,$a){
//        dd($request->all());
//    }
    public function sale(Request $request,$type){
      //dd('hello world');
        $table_id=session('table_id');
        $totalitem=$request->totalitems;
        $taxamount=$request->taxamount;
       $tax=str_replace('%','',$request->tax);
        $paid=$request->paid;
        $total=$request->total;
        //$discount=$request->discount;
        $discount=str_replace('%','',$request->discount);
        $discountamount=$request->discountamount;
        $created_by=$request->created_by;
        $status=$request->status;

        $sale=new Sall();
        $sale->totalitems=$totalitem;
        $sale->table_id=$table_id;
        $sale->taxamount=$taxamount;
        $sale->tax=$tax;
        $sale->paid=$paid;
        $sale->total=$total;
        $sale->discount=$discount;
        $sale->discountamount=$discountamount;
        $sale->status=$status;
        $sale->change=$paid - $total;
        $sale->created_by=$created_by;
        $sale->save();


        $sale_id=$sale->id;
        $temporaries = Temporary::Where('table_id',session('table_id'))
            ->whereDate('created_at',date('Y-m-d'))->get();


        foreach ($temporaries as $temporary){
            //$suptotal=$temporary->price*$temporary->qty;
          $price = $temporary->price;
          $qty = $temporary->qty;
          $subtotal=$price*$qty;
          $product_id = $temporary->product_id;
            $product=Product::find($product_id);

            SaleItem::insert([
                'product_id'=>$product_id,
                'sale_id'=>$sale_id,
                'qty'=>$qty,
                'price'=>$price,
                'product_name'=>$product->name,
                'subtotal'=>$subtotal
            ]);

        }

        Temporary::Where('table_id',session('table_id'))
            ->delete();


        return view('layout.payment');
    }





}
