<?php

namespace App\Http\Controllers\Admin;

use App\Models\ProductTransaction;
use App\Models\PurchaseItem;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PurchaseRequest as StoreRequest;
use App\Http\Requests\PurchaseRequest as UpdateRequest;

class PurchaseCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Purchase');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/purchase');
        $this->crud->setEntityNameStrings('purchase', 'purchases');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        //$this->crud->setFromDb();


        $this->crud->addColumn([
            'label' => 'Supplier',
            'name' => 'supplier_id',
            'type' =>  'select',
            'entity'=>  'supplier',
            'attribute'=>'name',
            'model'=>'App\Models\Purchase'
        ]);
        $this->crud->addField([
            // 1-n relationship
            'label' => "Supplier", // Table column heading
            'type' => "select2_from_ajax",
            'name' => 'supplier_id', // the column that contains the ID of that connected entity
            // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\\Models\\Supplier",
            'entity'=>  'supplier', // foreign key model
            'data_source' => url("api/supplier"), // url to controller search function (with /{id} should return model)
            'placeholder' => "Select a Supplier", // placeholder for the select
            'minimum_input_length' => 0, // minimum characters to type before querying results
            'attributes' => [
                'placeholder' => 'purchase number',
                'class' => 'form-control col-md-12'
            ],// extra HTML attributes and values your input might need
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ] // ext
        ]);

        $this->crud->addColumn([
            'name' =>'purchase_number',
            'label'=>'Purchase Number'
        ]);
        $this->crud->addField([
            'name' => 'purchase_number',
            'label' => 'Purchase Number',
            'attributes' => [
                'placeholder' => 'purchase number',
                'class' => 'form-control col-md-12'
            ],// extra HTML attributes and values your input might need
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ] // ext


        ]);
        $this->crud->addField([
            'name' => 'purchase_number',
            'label' => 'Purchase Number',
            'attributes' => [
                'placeholder' => 'purchase date',
                'class' => 'form-control col-md-12'
            ],// extra HTML attributes and values your input might need
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ] // ext
        ]);
        $this->crud->addColumn([
            'label' => 'Warehouse',
            'name' => 'warehouse_id',
            'type' =>  'select',
            'entity'=>  'warehouse',
            'attribute'=>'name',
            'model'=>'App\Models\Purchase'
        ]);
         $this->crud->addField([
             'label' => "Warehouse", // Table column heading
             'type' => "select2_from_ajax",
             'name' => 'warehouse_id', // the column that contains the ID of that connected entity
             // the method that defines the relationship in your Model
             'attribute' => "name", // foreign key attribute that is shown to user
             'model' => "App\\Models\\Warehouse",
             'entity'=>  'warehouse', // foreign key model
             'data_source' => url("api/warehouse"), // url to controller search function (with /{id} should return model)
             'placeholder' => "Select a Warehuse", // placeholder for the select
             'minimum_input_length' => 0, // minimum characters to type before querying results
             'attributes' => [
                 'placeholder' => 'purchase number',
                 'class' => 'form-control col-md-12'
             ],// extra HTML attributes and values your input might need
             'wrapperAttributes' => [
                 'class' => 'form-group col-md-6'
             ] // ext
         ]);
        $this->crud->addColumn([
            'name' => "purchase_date", // The db column name
            'label' => "Purchase Date", // Table column heading
            'type' => "datetime"
        ]);

        $this->crud->addField([
            'name' => 'purchase_date',
            'type' => 'date_picker',
            'label' => 'Purchase Date',
            'date_picker_options' => [
                'todayBtn' => true,
                'format' => 'dd-mm-yyyy',
                'language' => 'en'
            ],
            'attributes' => [
                'placeholder' => 'purchase number',
                'class' => 'form-control col-md-12'
            ],// extra HTML attributes and values your input might need
            'wrapperAttributes' => [
                'class' => 'form-group col-md-6'
            ] // ext
        ]);

        $this->crud->addField([
            'name' => "name",
            'attribute2' => "name",
            'label' => "Search Product ",
            'type' => 'select2-product',
            'model2' => '\App\Models\Product',
        ]);
        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'purchase'
        ]);


        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud-> addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here


        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
         $purchase_id=$this->crud->entry->id;
         $count_item=$request->price;
         $price=$request->price;
         $product_id=$request->product_id;
         $qty=$request->qty;
         $discount=$request->discount;
         $subtotal=$request->subtotal;
         $i = 0;
         foreach ($count_item as $k => $v){
             $m = new PurchaseItem();
             $m->purchase_id=$purchase_id;
             $m->price = $price[$i];
             $m->product_id = $product_id[$i];
             $m->discount = $discount[$i];
             $m->qty = $qty[$i];
             $m->subtotal = $subtotal[$i];
             $m->save();

             //===================================
             $tra=new ProductTransaction();
             $tra->product_id=$product_id[$i];
             $tra->refenrece_id=$purchase_id;
             $tra->warehouse_id=$request->warehouse_id;
             $tra->train_date=$request->purchase_date;
             $tra->quantity = $qty[$i];
             $tra->refenrece_type = 'purchase';
             $tra->discount = $discount[$i];
             $tra->price = $price[$i];
             $tra->save();
             $i++;
         }



        $prochese_id=$this->crud->entry->id;


        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function destroy($id)
    {
        $this->crud->hasAccessOrFail('delete');

        // get entry ID from Request (makes sure its the last ID for nested resources)
        $id = $this->crud->getCurrentEntryId() ?? $id;
           PurchaseItem::where('purchase_id',$id)->delete();
           ProductTransaction::where('refenrece_id',$id)->delete();
        return $this->crud->delete($id);
    }
}
