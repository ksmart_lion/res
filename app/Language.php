<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Language
 *
 * @mixin \Eloquent
 */
class Language extends Model
{
    protected $table = 'languages';
}
