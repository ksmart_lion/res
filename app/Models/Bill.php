<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/**
 * App\Models\Bill
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $table_id
 * @property string|null $reference_no
 * @property string|null $table_name
 * @property int|null $grand_total
 * @property int|null $total
 * @property int|null $discount
 * @property int|null $total_items
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereGrandTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereReferenceNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereTableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereTableName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereTotalItems($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Bill whereUserId($value)
 * @mixin \Eloquent
 */
class Bill extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'bills';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
