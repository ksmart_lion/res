<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/**
 * App\Models\WarehouseQuatity
 *
 * @property int $id
 * @property int|null $product_id
 * @property int|null $warehouse_id
 * @property string|null $description
 * @property string $quantity
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Product|null $product
 * @property-read \App\Models\Warehouse|null $warehouse
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WarehouseQuatity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WarehouseQuatity whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WarehouseQuatity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WarehouseQuatity whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WarehouseQuatity whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WarehouseQuatity whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WarehouseQuatity whereWarehouseId($value)
 * @mixin \Eloquent
 */
class WarehouseQuatity extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'warehouse_quatities';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['warehouse_id','product_id','description','quantity'];
    // protected $hidden = [];
    // protected $dates = [];
    public  function product(){
        return $this->belongsTo('App\Models\Product','product_id');
    }
    public  function warehouse(){
        return $this->belongsTo('App\Models\Warehouse','warehouse_id');
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
