<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SaleProduct
 *
 * @property int $id
 * @property int $totalitems
 * @property float $taxamount
 * @property float $tax
 * @property float $paid
 * @property float $total
 * @property float $discount
 * @property float $subtotal
 * @property string $created_by
 * @property int $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleProduct whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleProduct whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleProduct wherePaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleProduct whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleProduct whereSubtotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleProduct whereTax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleProduct whereTaxamount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleProduct whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleProduct whereTotalitems($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleProduct whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SaleProduct extends Model
{
    protected $table='sale_products';
    protected $fillable=['totalitems','taxamount','tax','paid','total','subtotal',
        'discount','discountamount','created_by','status','ccnum','ccmonth','ccyear',
        'ccv'];
}
