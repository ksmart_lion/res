<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/**
 * App\Models\PosRes
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $table_id
 * @property float|null $totalitems
 * @property float|null $discountamount
 * @property float|null $taxamount
 * @property float|null $tax
 * @property float|null $paid
 * @property float|null $total
 * @property float|null $discount
 * @property string|null $created_by
 * @property int|null $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosRes whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosRes whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosRes whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosRes whereDiscountamount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosRes whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosRes wherePaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosRes whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosRes whereTableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosRes whereTax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosRes whereTaxamount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosRes whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosRes whereTotalitems($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosRes whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PosRes whereUserId($value)
 * @mixin \Eloquent
 */
class PosRes extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'sales';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['user_id','table_id','reference_no','table_name','grand_total','total','discount','total_items'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
