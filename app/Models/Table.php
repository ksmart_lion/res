<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/**
 * App\Models\Table
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $warehouse_id
 * @property string|null $name
 * @property string|null $description
 * @property int|null $number_person
 * @property int $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Table whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Table whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Table whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Table whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Table whereNumberPerson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Table whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Table whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Table whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Table whereWarehouseId($value)
 * @mixin \Eloquent
 */
class Table extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'tables';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['name','description','number_person','user_id'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
