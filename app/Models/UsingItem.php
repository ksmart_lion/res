<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UsingItem
 *
 * @property int $id
 * @property int|null $using_id
 * @property int|null $product_id
 * @property int|null $qty
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsingItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsingItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsingItem whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsingItem whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsingItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UsingItem whereUsingId($value)
 * @mixin \Eloquent
 */
class UsingItem extends Model
{
    //
    protected $table = 'using_items';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['product_id','using_id','qty'];
}
