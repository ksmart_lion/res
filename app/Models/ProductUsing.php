<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/**
 * App\Models\ProductUsing
 *
 * @property int $id
 * @property int|null $warehouse_id
 * @property int|null $user_id
 * @property string|null $note
 * @property string|null $using_date
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Warehouse|null $warehouse
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductUsing whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductUsing whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductUsing whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductUsing whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductUsing whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductUsing whereUsingDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductUsing whereWarehouseId($value)
 * @mixin \Eloquent
 */
class ProductUsing extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'using';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['warehouse_id','user_id','using_date','note'];
    public  function warehouse(){
        return $this->belongsTo('App\Models\Warehouse','warehouse_id');
    }
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
