<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Temporary
 *
 * @property int $id
 * @property int $product_id
 * @property int $table_id
 * @property int $qty
 * @property float $price
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Temporary whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Temporary whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Temporary wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Temporary whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Temporary whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Temporary whereTableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Temporary whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Temporary extends Model
{
    //
    protected $table='temporaries';
    protected $fillable=['product_id','table_id','qty','price'];
}
