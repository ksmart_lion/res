<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/**
 * App\Models\Purchase
 *
 * @property int $id
 * @property int|null $supplier_id
 * @property int|null $warehouse_id
 * @property string|null $purchase_number
 * @property string|null $purchase_date
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Supplier|null $supplier
 * @property-read \App\Models\Warehouse|null $warehouse
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Purchase whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Purchase whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Purchase wherePurchaseDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Purchase wherePurchaseNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Purchase whereSupplierId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Purchase whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Purchase whereWarehouseId($value)
 * @mixin \Eloquent
 */
class Purchase extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'purchases';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['supplier_id','purchase_number','purchase_date','warehouse_id'];
    // protected $hidden = [];
    // protected $dates = [];
//       protected  $appends=[
//           'po_search'
//       ];

    public  function supplier(){
        return $this->belongsTo('App\Models\Supplier','supplier_id');
    }
    public  function warehouse(){
        return $this->belongsTo('App\Models\Warehouse','warehouse_id');
    }
//    public  function purchase_item(){
//        return $this->hasMany('App\Models\PurchaseItem','purchase_id');
//    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
