<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProductTransaction
 *
 * @property int $id
 * @property int|null $product_id
 * @property int|null $warehouse_id
 * @property int|null $refenrece_id
 * @property string|null $refenrece_type
 * @property int|null $quantity
 * @property float|null $price
 * @property float|null $discount
 * @property string|null $train_date
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductTransaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductTransaction whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductTransaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductTransaction wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductTransaction whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductTransaction whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductTransaction whereRefenreceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductTransaction whereRefenreceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductTransaction whereTrainDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductTransaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductTransaction whereWarehouseId($value)
 * @mixin \Eloquent
 */
class ProductTransaction extends Model
{

    protected $table = 'product_transaction';
    protected $fillable=['product_id','warehouse_id','refenrece_id','train_date','refenrece_type','quantity','price','discount'];


    //
}
