<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SaleItem
 *
 * @property int $id
 * @property int|null $product_id
 * @property int|null $sale_id
 * @property string|null $product_name
 * @property int|null $price
 * @property int|null $qty
 * @property int|null $subtotal
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleItem whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleItem whereProductName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleItem whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleItem whereSaleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleItem whereSubtotal($value)
 * @mixin \Eloquent
 */
class SaleItem extends Model
{
    //

    protected $table='sale_items';
    protected $fillable=['product_id','sale_id','product_name','qty','price','suptotal'];
}
