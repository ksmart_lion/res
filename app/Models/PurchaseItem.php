<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/**
 * App\Models\PurchaseItem
 *
 * @property int $id
 * @property int|null $purchase_id
 * @property int|null $product_id
 * @property string|null $no
 * @property int|null $qty
 * @property float|null $discount
 * @property float|null $subtotal
 * @property float|null $price
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Purchase[] $purchase
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseItem whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseItem whereNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseItem wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseItem whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseItem wherePurchaseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseItem whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseItem whereSubtotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PurchaseItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PurchaseItem extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'purchase_items';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = ['product_id','purchase_id','qty','discount','subtotal'];
    // protected $hidden = [];
    // protected $dates = [];

    public  function purchase(){
        return $this->hasMany('App\Models\Purchase','purchase_id');
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
