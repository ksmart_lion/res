<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

/**
 * App\Models\Sall
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $table_id
 * @property float|null $totalitems
 * @property float|null $discountamount
 * @property float|null $taxamount
 * @property float|null $tax
 * @property float|null $paid
 * @property float|null $total
 * @property float|null $discount
 * @property string|null $created_by
 * @property int|null $status
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sall whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sall whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sall whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sall whereDiscountamount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sall whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sall wherePaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sall whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sall whereTableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sall whereTax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sall whereTaxamount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sall whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sall whereTotalitems($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sall whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sall whereUserId($value)
 * @mixin \Eloquent
 */
class Sall extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'sales';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable=['totalitems','taxamount','paid','total',
        'discount','discountamount','created_by','status','table_id'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
