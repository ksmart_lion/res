<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(url('/admin/login'));
});
//Route::get('/home', function () {
//    return redirect(url('/admin/dashboard'));
//});
Route::get('/home', function () {
     return view('layout.dashboard');
});


Route::get('/lang/{l}', function ($l) {
    $arr_lang = ['en', 'km'];
    $la = in_array($l, $arr_lang) ? $l : 'en';
    session(['sess_lang' => $la]);
    return redirect()->back();
});

Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function()
{
  // Backpack\CRUD: Define the resources for the entities you want to CRUD.
     CRUD::resource('category', 'Admin\CategoryCrudController');
     CRUD::resource('product', 'Admin\ProductCrudController');
     CRUD::resource('table', 'Admin\TableCrudController');
     CRUD::resource('warehouse', 'Admin\WarehouseCrudController');
     CRUD::resource('warehousequatity', 'Admin\WarehouseQuatityCrudController');
     CRUD::resource('posres', 'Admin\PosResCrudController');
     CRUD::resource('tableform', 'Admin\TableFormCrudController');
     CRUD::resource('supplier', 'Admin\SupplierCrudController');
     CRUD::resource('purchase', 'Admin\PurchaseCrudController');
     CRUD::resource('productusing', 'Admin\ProductUsingCrudController');
     CRUD::resource('customer', 'Admin\CustomerCrudController');
  
  // [...] other routes
});
Route::get('/api/category', 'Api\CategoryController@index');
Route::get('/api/category/{id}', 'Api\CategoryController@show');

Route::get('/api/supplier', 'Api\SupplierController@index');
Route::get('/api/supplier/{id}', 'Api\SupplierController@show');

Route::get('/api/warehouse', 'Api\WarehouseQuatityController@index');
Route::get('/api/warehouse/{id}', 'Api\WarehouseQuatityController@show');
Route::get('/api/using', 'Api\ProductUsingController@index');
Route::get('/api/using/{id}', 'Api\ProductUsingController@show');

Route::get('/api/product', 'Api\ProductQuatityController@index');
Route::get('/api/product/{id}', 'Api\ProductQuatityController@show');
Route::get('/api/warehouse', 'Api\WarehouseController@index');
Route::get('/api/warehouse/{id}', 'Api\WarehouseController@show');


//======= rest pos

//Route::get('/pos/load_posales', function (){
//    return view('order.pos.load_posales');
//});



Route::get('/pos/addpdc', 'Api\SelectController@select');
Route::get('/pos/load_posales', 'Api\SelectController@posale');


//Route::get('/pos/totiems', function (){
//
//    return 18888888888;
//});

Route::get('/pos/totiems', 'Api\SelectController@total_item');
Route::get('/pos/subtot', 'Api\SelectController@subtotal');
//
//Route::get('/pos/subtot', function (){
//    return  10;
//});

Route::get('/pos/AddHold/{a}', function ($a){
    return response()->json(['status'=>true]);
});
Route::get('/pos/ResetPos/', 'Api\SelectController@reset_pos');
Route::get('/pos/holdList/{a}', function ($a){
    return view('order.pos.holdList');
});

Route::get('/pos/WaiterName/{a}', function ($a){
    return 1;
});

Route::get('/pos/CustomerName/{a}', function ($a){
    return 0;
});

Route::get('/pos/changewaiterS/', function (){
    return response()->json(['status'=>true]);
});

Route::get('/pos/changecustomerS/', function (){
    return response()->json(['status'=>true]);
});

Route::get('/pos/showticket/{a}/{b}/{c?}/{d?}', function ($a,$b,$c=null,$d=null){
    return view('order.pos.showticket',['hold'=>$a,'total'=>$b,'taxValue'=>$c,'RemiseValue'=>$d]);
});

Route::get('/pos/AddNewSale/{a}', function ($a){
    return view('layout.payment');
});

Route::get('/pos/AddNewSale/{a}','Api\SelectController@sale');

Route::get('pos/SelectHold/{a}', function ($a){
    session([
        'table_id'=> $a
    ]);

    return response()->json(['status'=>true]);
});

Route::get('/pos/GetDiscount/{a}', function ($a){
    return '8%~'.$a;
});

Route::get('/pos/RemoveHold/{a}/{b}', function ($a,$b){
    return response()->json(['status'=>true]);
});

Route::get('/pos/CloseTable/', function (){
    return response()->json(['status'=>true]);
});

Route::get('/pos/getoptions/{a}/{b}', function ($a,$b){
    return response()->json(['status'=>true]);
});

//Route::get('pos/edit/{a}', function ($a){
//    return response()->json(['status'=>true]);
//});
//Route::get('pos/delete/{a}', function ($a){
//    return response()->json(['status'=>true]);
//});

Route::get('pos/edit/{id}', 'Api\SelectController@update_qty');
Route::get('pos/delete/{id}', 'Api\SelectController@remove_item');


Route::get('/pos/dashboardpos', function (){
    return view('layout.dashboard');
});

Route::get('/reports/getCustomerReport', function (){
    return "hello world";
   // return view('layout.customerreport');
});


