<div class="col-xs-12">
    <div class="panel panel-default product-details">
        <div class="panel-body" style="">
            <div class="col-xs-5 nopadding">
                <div class="col-xs-2 nopadding">
                    <a href="javascript:void(0)" onclick="delete_posale('1932')">
                        <span class="fa-stack fa-sm productD"><i
                                    class="fa fa-circle fa-stack-2x delete-product"></i><i
                                    class="fa fa-times fa-stack-1x fa-fw fa-inverse"></i>
                        </span></a>
                </div>
                <div class="col-xs-10 nopadding">
                    <span class="textPD">Black coffee</span></div>
            </div>
            <div class="col-xs-2"><span class="textPD">2.000</span></div>
            <div class="col-xs-3 nopadding productNum"><a href="javascript:void(0)">
                    <span class="fa-stack fa-sm decbutton">
                        <i class="fa fa-square fa-stack-2x light-grey"></i>
                        <i class="fa fa-minus fa-stack-1x fa-inverse white"></i></span></a>
                <input type="text" id="qt-1932" onchange="edit_posale(1932)" class="form-control" value="2" placeholder="0"
                                                                                                       maxlength="3"><a
                        href="javascript:void(0)"><span class="fa-stack fa-sm incbutton">
                        <i class="fa fa-square fa-stack-2x light-grey"></i>
                        <i class="fa fa-plus fa-stack-1x fa-inverse white"></i></span></a></div>
            <div class="col-xs-2 nopadding "><span class="subtotal textPD">4.000  MXN</span></div>
        </div>
        <button type="button" onclick="addoptions(148, 1932)" class="btn btn-success btn-xs">opciones</button>
        <span id="pooptions-1932"> </span></div>
</div>


<script type="text/javascript">
    $(".incbutton").on("click", function () {
        var $button = $(this);
        var oldValue = $button.parent().parent().find("input").val();
        var newVal = parseFloat(oldValue) + 1;
        $button.parent().parent().find("input").val(newVal);
        edit_posale($button.parent().parent().find("input").attr("id").slice(3));
    });
    $(".decbutton").on("click", function () {
        var $button = $(this);
        var oldValue = $button.parent().parent().find("input").val();
        if (oldValue > 1) {
            var newVal = parseFloat(oldValue) - 1;
        } else {
            newVal = 1;
        }
        $button.parent().parent().find("input").val(newVal);
        edit_posale($button.parent().parent().find("input").attr("id").slice(3));
    });</script>