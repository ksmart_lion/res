<table class="table table-bordered">
    <thead >
    <tr style="background-color:#7274ff;color:white">
        <th>NO</th>
        <th>Product Name</th>
        <th>Price($)</th>
        <th>Quantity</th>
        <th>Discount (%)</th>
        <th>Subtotal</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody id="purchase">
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5" style="text-align: right">Total</td>
            <td><input type="text" class="form-control total" name="total"></td>
            <td></td>
        </tr>
    </tfoot>
</table>
@push('crud_fields_scripts')
<script>
    $(function () {
        $('body').on('keyup','.price,.qty,.discount,.subtotal',function () {
            var tr=$(this).parent().parent();
           // alert(tr);
            var price=tr.find('.price').val();
            var qty=tr.find('.qty').val();
            var discount=tr.find('.discount').val();
            //var
            var amount = price*qty*(1-(discount/100));
            tr.find('.subtotal').val(amount);
            var total_all = subtotal();
            $('.total').val(total_all);

        });
        $('body').on('click','.remove-product',function () {
            var tr=$(this).parent().parent();
                tr.remove();
            var total_all = subtotal();
            $('.total').val(total_all);

        });
    });

    function subtotal() {
        var total = 0;
        $('.subtotal').each(function () {

            var t = $(this).val()-0;

            total += t;
        });

        return total;
    }
</script>
@endpush
