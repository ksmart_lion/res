<div class="col-md-12">
    <div class="text-center"><p><br></p>
        <p>
            <span  style="font-family: Tahoma, 'Lucida Grande', 'Trebuchet MS', Verdana, Helvetica, sans-serif; font-size: 15.4px; font-style: normal; font-variant: normal; font-weight: normal; line-height: normal;">KHMER Restaurant</span><br>
        </p>
    </div>
    <?php
     $subtotal = 0;
    ?>
    <div style="clear:both;"><br>
        <div style="clear:both;">
            <div style="clear:both;"><span class="float-left">

                    <?php
                    echo date("Y/m/d h:i:sa");
                    ?>
                </span>
                <?php
                $table_ = \App\Models\Table::find(session('table_id'));
                ?>
                <span class="float-right">Table :{{optional($table_)->name}}</span>

                <div style="clear:both;"><span class="float-left"><br> </span>
                    <div style="clear:both;">
                        <table class="table" cellspacing="0" border="0">
                            <thead>
                            <tr>
                                <th><em>#</em></th>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>SubTotal</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(\App\Models\Temporary::where('table_id',session('table_id'))->whereDate('created_at',date('Y-m-d'))->get() as $index=> $tem )
                                <?php
                                $select = \App\Models\Product::find($tem->product_id);
                                ?>
                                <tr>
                                    <td style="text-align:center; width:30px;">{{$index+1}}</td>
                                    <td style="text-align:left; width:180px;">{{$select->name}}<br><span
                                                style="font-size:12px;color:#666"></span></td>
                                    <td style="text-align:center; width:50px;">{{$tem->qty}}</td>
                                    <td style="text-align:right; width:70px;font-size:14px; ">
                                        <?php

                                        echo $tem->qty * $tem->price;
                                        $subtotal += $tem->qty * $tem->price;
                                        ?>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <table class="table" cellspacing="0" border="0" style="margin-bottom:8px;">
                            <tbody>
                            <tr>
                                <td style="text-align:left;">Total Items</td>
                                <td style="text-align:right; padding-right:1.5%;">
                                    <?php
                                    $item = \App\Models\Temporary::where('table_id', session('table_id'))
                                        ->whereDate('created_at', date('Y-m-d'))
                                        ->sum('qty');
                                    echo $item;
                                    ?>
                                </td>


                                <td style="text-align:left; padding-left:1.5%;">Total</td>
                                <td style="text-align:right;font-weight:bold;">
                                    <?php
                                    echo $subtotal;
                                    ?>
                                </td>
                            </tr>


                            <tr>
                                <td colspan="2" style="text-align:left; font-weight:bold; padding-top:5px;">Discount</td>
                                <td colspan="2" style="border-top:1px dashed #000; padding-top:5px; text-align:right; font-weight:bold;">
                                    <?php
                                    echo isset($RemiseValue)?$RemiseValue:0;
                                    ?>

                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align:left; font-weight:bold; padding-top:5px;">Tax Total</td>
                                <td colspan="2" style="border-top:1px dashed #000; padding-top:5px; text-align:right; font-weight:bold;">
                                    <?php
                                    echo isset($taxValue)?$taxValue:0
                                    ?>

                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="text-align:left; font-weight:bold; padding-top:5px;">Grand Total</td>
                                <td colspan="2" style="border-top:1px dashed #000; padding-top:5px; text-align:right; font-weight:bold;">
                                    <?php
                                    echo isset($total)? ($total):0;
                                    ?>
                                    $
                                </td>
                            </tr>

                            </tbody>
                        </table>
                        <div style="border-top:1px solid #000; padding-top:10px;">
                            <span class="float-left">CVIT</span><span class="float-right">Tel: 085 555 977</span>
                            <div style="clear:both;">
                                <p class="text-center" style="margin:0 auto;margin-top:10px;"></p>
                                <div class="text-center"
                                     style="background-color:#000;padding:5px;width:85%;color:#fff;margin:0 auto;border-radius:3px;margin-top:20px;">
                                    Thank you for your business
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
