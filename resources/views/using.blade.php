<table class="table table-bordered">
    <thead >
    <tr style="background-color:#7274ff;color:white">
        <th>NO</th>
        <th>Product Name</th>
        <th>Quantity</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody id="purchase">
    </tbody>
</table>
@push('crud_fields_scripts')
<script>
    $(function () {
        $('body').on('keyup','.qty',function () {
            var tr=$(this).parent().parent();
            var qty=tr.find('.qty').val();
        });
        $('body').on('click','.remove-product',function () {
            var tr=$(this).parent().parent();
            tr.remove();
        });
    });

</script>
@endpush
