<table id="Table" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Number</th>
        <th>Customer Name</th>
        <th>Discount</th>
        <th>Total</th>
        <th>Ceated By</th>
        <th>Total Items</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>35</td>
        <td>Walk in Customer</td>
        <td></td>
        <td>57.680 USD</td>
        <td>admin Doe</td>
        <td>4</td>
    </tr>
    <tr>
        <td>36</td>
        <td>Walk in Customer</td>
        <td></td>
        <td>48.720 USD</td>
        <td>admin Doe</td>
        <td>4</td>
    </tr>
    <tr>
        <td>37</td>
        <td>Tiffany Green</td>
        <td></td>
        <td>20.930 USD</td>
        <td>admin Doe</td>
        <td>2</td>
    </tr>
    <tr>
        <td>38</td>
        <td>Walk in Customer</td>
        <td></td>
        <td>26.880 USD</td>
        <td>admin Doe</td>
        <td>4</td>
    </tr>
    <tr>
        <td>39</td>
        <td>Cliente sin Registrar</td>
        <td></td>
        <td>22.400 USD</td>
        <td>admin Doe</td>
        <td>1</td>
    </tr>
    <tr>
        <td>40</td>
        <td>Walk in Customer</td>
        <td></td>
        <td>2.240 USD</td>
        <td>admin Doe</td>
        <td>1</td>
    </tr>
    <tr>
        <td>41</td>
        <td>Cliente sin Registrar</td>
        <td></td>
        <td>22.400 USD</td>
        <td>admin Doe</td>
        <td>1</td>
    </tr>
    <tr>
        <td>42</td>
        <td>Khách ngoài</td>
        <td></td>
        <td>47.040 USD</td>
        <td>admin Doe</td>
        <td>3</td>
    </tr>
    <tr>
        <td>45</td>
        <td>Walk in Customer</td>
        <td></td>
        <td>22.960 USD</td>
        <td>admin Doe</td>
        <td>2</td>
    </tr>
    <tr>
        <td>46</td>
        <td>Walk in Customer</td>
        <td></td>
        <td>22.400 USD</td>
        <td>admin Doe</td>
        <td>1</td>
    </tr>
    <tr>
        <td>47</td>
        <td>Walk in Customer</td>
        <td></td>
        <td>0.000 USD</td>
        <td>admin Doe</td>
        <td>0</td>
    </tr>
    <tr>
        <td>48</td>
        <td>Walk in Customer</td>
        <td></td>
        <td>33.600 USD</td>
        <td>admin Doe</td>
        <td>4</td>
    </tr>
    <tr>
        <td>50</td>
        <td>Walk in Customer</td>
        <td></td>
        <td>43.680 USD</td>
        <td>admin Doe</td>
        <td>4</td>
    </tr>
    <tr>
        <td>51</td>
        <td>Walk in Customer</td>
        <td></td>
        <td>61.040 USD</td>
        <td>admin Doe</td>
        <td>7</td>
    </tr>
    <tr>
        <td>53</td>
        <td>Gregory Hawkins</td>
        <td></td>
        <td>-12.000 USD</td>
        <td>admin Doe</td>
        <td>0</td>
    </tr>
    <tr>
        <td>54</td>
        <td>Walk in Customer</td>
        <td></td>
        <td>22.000 USD</td>
        <td>admin Doe</td>
        <td>2</td>
    </tr>
    <tr>
        <td>56</td>
        <td>Müşteri yürümek</td>
        <td></td>
        <td>41.500 USD</td>
        <td>admin Doe</td>
        <td>4</td>
    </tr>
    <tr>
        <td>58</td>
        <td>Walk in Customer</td>
        <td></td>
        <td>72.500 USD</td>
        <td>admin Doe</td>
        <td>12</td>
    </tr>
    <tr>
        <td>59</td>
        <td>Walk in Customer</td>
        <td></td>
        <td>153.000 USD</td>
        <td>admin Doe</td>
        <td>14</td>
    </tr>
    <tr>
        <td>60</td>
        <td>Walk in Customer</td>
        <td></td>
        <td>199.000 USD</td>
        <td>admin Doe</td>
        <td>22</td>
    </tr>
    </tbody>
</table>
<h1>Total : <span class="ReportTotal">907.97 USD</span></h1>