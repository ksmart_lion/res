<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>POS - point of sale Dar Elweb</title>
    <!-- jQuery -->
    <script type="text/javascript" src="{{ $rest_url }}/zarest/assets/js/jquery-2.2.2.min.js"></script>
    <script type="text/javascript" src="{{ $rest_url }}/zarest/assets/js/loading.js"></script>
    <!-- normalize & reset style -->
    <link rel="stylesheet" href="{{ $rest_url }}/zarest/assets/css/normalize.min.css" type='text/css'>
    <link rel="stylesheet" href="{{ $rest_url }}/zarest/assets/css/reset.min.css" type='text/css'>
    <link rel="stylesheet" href="{{ $rest_url }}/zarest/assets/css/jquery-ui.css" type='text/css'>
    <!-- google lato/Kaushan/Pinyon fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,300' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pinyon+Script" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link href="{{ $rest_url }}/zarest/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap-horizon -->
    <link href="{{ $rest_url }}/zarest/assets/css/bootstrap-horizon.css" rel="stylesheet">
    <!-- datatable style -->
    <link href="{{ $rest_url }}/zarest/assets/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
    <!-- font awesome -->
    <link rel="stylesheet" href="{{ $rest_url }}/zarest/assets/css/font-awesome.min.css">
    <!-- include summernote css-->
    <link href="{{ $rest_url }}/zarest/assets/css/summernote.css" rel="stylesheet">
    <!-- waves -->
    <link rel="stylesheet" href="{{ $rest_url }}/zarest/assets/css/waves.min.css">
    <!-- daterangepicker -->
    <link rel="stylesheet" type="text/css" href="{{ $rest_url }}/zarest/assets/css/daterangepicker.css"/>
    <!-- css for the preview keyset extension -->
    <link href="{{ $rest_url }}/zarest/assets/css/keyboard-previewkeyset.css" rel="stylesheet">
    <!-- keyboard widget style -->
    <link href="{{ $rest_url }}/zarest/assets/css/keyboard.css" rel="stylesheet">
    <!-- Select 2 style -->
    <link href="{{ $rest_url }}/zarest/assets/css/select2.min.css" rel="stylesheet">
    <!-- Sweet alert swal -->
    <link rel="stylesheet" type="text/css" href="{{ $rest_url }}/zarest/assets/css/sweetalert.css">
    <!-- datepicker css -->
    <link rel="stylesheet" type="text/css" href="{{ $rest_url }}/zarest/assets/css/bootstrap-datepicker.min.css">
    <!-- Custom CSS -->
    <link href="{{ $rest_url }}/zarest/assets/css/Style-Light.css" rel="stylesheet">
    <!-- favicon -->
    <link rel="shortcut icon" href="{{ $rest_url }}/zarest//favicon.ico?v=2" type="image/x-icon">
    <link rel="icon" href="{{ $rest_url }}/zarest//favicon.ico?v=2" type="image/x-icon">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- Navigation -->
{{--<nav class="navbar navbar-default navbar-fixed-top" role="navigation">--}}
    {{--<div class="container-fluid">--}}
        {{--<div class="navbar-header">--}}
            {{--<button type="button" class="navbar-toggle collapsed" data-toggle="collapse"--}}
                    {{--data-target="#bs-example-navbar-collapse-1" aria-expanded="false">--}}
                {{--<span class="sr-only">Toggle navigation</span>--}}
                {{--<span class="icon-bar"></span>--}}
                {{--<span class="icon-bar"></span>--}}
                {{--<span class="icon-bar"></span>--}}
            {{--</button>--}}
            {{--<a class="navbar-brand" href="#"><img--}}
                        {{--src="{{ $rest_url }}/zarest/files/Setting/453cf07a7df88ad35a65bd0e181dccf6.png" alt="logo"--}}
                        {{--style='max-height: 45px; max-width: 200px;margin-top:5px;'></a>--}}
        {{--</div>--}}
        {{--<!-- Brand and toggle get grouped for better mobile display -->--}}
        {{--<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">--}}
            {{--<ul class="nav navbar-nav">--}}
                {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/"><i class="fa fa-credit-card"></i> <span--}}
                                {{--class="menu-text">POS</span></a></li>--}}
                {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/products"><i class="fa fa-archive"></i> <span--}}
                                {{--class="menu-text">Product</span></a></li>--}}
                {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/stores"><i class="fa fa-hospital-o"></i> <span--}}
                                {{--class="menu-text">Stores</span></a></li>--}}
                {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle flat-box" data-toggle="dropdown" role="button"--}}
                       {{--aria-haspopup="true" aria-expanded="false"><i class="fa fa-users"></i> <span class="menu-text">People</span>--}}
                        {{--<span class="caret"></span></a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/waiters"><i class="fa fa-user"></i> <span--}}
                                        {{--class="menu-text">Waiters</span></a></li>--}}
                        {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/customers"><i class="fa fa-user"></i> <span--}}
                                        {{--class="menu-text">Customers</span></a></li>--}}
                        {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/suppliers"><i class="fa fa-truck"></i>--}}
                                {{--<span class="menu-text">Suppliers</span></a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/sales"><i class="fa fa-ticket"></i> <span--}}
                                {{--class="menu-text">Sales</span></a></li>--}}
                {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/expences"><i class="fa fa-usd"></i> <span--}}
                                {{--class="menu-text">Expense</span></a></li>--}}
                {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle flat-box" data-toggle="dropdown" role="button"--}}
                       {{--aria-haspopup="true" aria-expanded="false"><i class="fa fa-bookmark"></i> <span--}}
                                {{--class="menu-text">Categories </span><span class="caret"></span></a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/categories"><i class="fa fa-archive"></i>--}}
                                {{--<span class="menu-text">Product</span></a></li>--}}
                        {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/categorie_expences"><i--}}
                                        {{--class="fa fa-usd"></i> <span class="menu-text">Expense</span></a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            {{--</ul>--}}
            {{--<ul class="nav navbar-nav navbar-right">--}}
                {{--<li><a href="">--}}
                        {{--<img class="img-circle topbar-userpic hidden-xs"--}}
                             {{--src="{{ $rest_url }}/zarest/files/Avatars/3b49aefa789efb5e6db62d5ae906a4c2.jpg"--}}
                             {{--width="30px" height="30px">--}}
                        {{--<span class="hidden-xs"> &nbsp;&nbsp;sale Staff </span>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="dropdown language">--}}
                    {{--<a href="#" class="dropdown-toggle flat-box" data-toggle="dropdown" role="button"--}}
                       {{--aria-haspopup="true" aria-expanded="false">--}}
                        {{--<img src="{{ $rest_url }}/zarest/assets/img/flags/en.png" class="flag" alt="language">--}}
                        {{--<span class="caret"></span></a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/dashboard/change/english"><img--}}
                                        {{--src="{{ $rest_url }}/zarest/assets/img/flags/en.png" class="flag"--}}
                                        {{--alt="language"> English</a></li>--}}
                        {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/dashboard/change/francais"><img--}}
                                        {{--src="{{ $rest_url }}/zarest/assets/img/flags/fr.png" class="flag"--}}
                                        {{--alt="language"> Français</a></li>--}}
                        {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/dashboard/change/portuguese"><img--}}
                                        {{--src="{{ $rest_url }}/zarest/assets/img/flags/pr.png" class="flag"--}}
                                        {{--alt="language"> Portuguese</a></li>--}}
                        {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/dashboard/change/spanish"><img--}}
                                        {{--src="{{ $rest_url }}/zarest/assets/img/flags/sp.png" class="flag"--}}
                                        {{--alt="language"> Spanish</a></li>--}}
                        {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/dashboard/change/arabic"><img--}}
                                        {{--src="{{ $rest_url }}/zarest/assets/img/flags/ar.png" class="flag"--}}
                                        {{--alt="language"> العربية</a></li>--}}
                        {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/dashboard/change/danish"><img--}}
                                        {{--src="{{ $rest_url }}/zarest/assets/img/flags/da.png" class="flag"--}}
                                        {{--alt="language"> Danish</a></li>--}}
                        {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/dashboard/change/turkish"><img--}}
                                        {{--src="{{ $rest_url }}/zarest/assets/img/flags/tr.png" class="flag"--}}
                                        {{--alt="language"> Turkish</a></li>--}}
                        {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/dashboard/change/greek"><img--}}
                                        {{--src="{{ $rest_url }}/zarest/assets/img/flags/gr.png" class="flag"--}}
                                        {{--alt="language"> Greek</a></li>--}}
                        {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/dashboard/change/vietnam"><img--}}
                                        {{--src="{{ $rest_url }}/zarest/assets/img/flags/vn.png" class="flag"--}}
                                        {{--alt="language"> Vietnam</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li class="flat-box"><a href="{{ $rest_url }}/zarest/logout" title="Logout"><i--}}
                                {{--class="fa fa-sign-out fa-lg"></i></a></li>--}}
            {{--</ul>--}}
        {{--</div>--}}
        {{--<div id="loadingimg"></div>--}}
    {{--</div>--}}
    {{--<!-- /.container -->--}}
{{--</nav>--}}
{{--<!-- Page Content -->--}}


<!-- Page Content -->

<!-- *************************************************** if a table was choosen ********************************** -->
    {{--<div class="row text-center">--}}
        {{--<h3 style="font-family: 'Kaushan Script', cursive;">Table - 4</h3>--}}
    {{--</div>--}}
    <div class="row" >
        <ul class="cbp-vimenu2">
            <li data-toggle="tooltip" data-html="true" data-placement="left" title="Cancel&nbsp;All"><a
                        href="javascript:void(0)" onclick="CloseTable()"><i class="fa fa-times" aria-hidden="true"></i></a>
            </li>
            <li data-toggle="tooltip" data-html="true" data-placement="left" title="Return"><a href="pos/switshtable"><i
                            class="fa fa-reply" aria-hidden="true"></i></a></li>
            <li data-toggle="tooltip" data-html="true" data-placement="left" title="Go&nbsp;to&nbsp;Kitchen&nbsp;page">
                <a href="kitchens"><i class="fa fa-cutlery" aria-hidden="true"></i></a></li>
        </ul>
        <div class="col-md-5 left-side">
            <div class="row">
                <div class="row row-horizon">
               <span class="holdList">
                  <!-- list Holds goes here -->
               </span>
                    <span class="Hold pl" onclick="AddHold()">+</span>
                    <span class="Hold pl" onclick="RemoveHold()">-</span>
                </div>
            </div>
            <div class="col-xs-8">
                <h2>Choose Client</h2>
            </div>
            <div class="col-xs-4 client-add">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#AddCustomer">
               <span class="fa-stack fa-lg" data-toggle="tooltip" data-placement="top" title="Add New Customer">
                  <i class="fa fa-square fa-stack-2x grey"></i>
                  <i class="fa fa-user-plus fa-stack-1x fa-inverse dark-blue"></i>
               </span>
                </a>
                <a href="javascript:void(0)" onclick="showticket()">
               <span class="fa-stack fa-lg" data-toggle="tooltip" data-placement="top" title="Show last Receipt">
                  <i class="fa fa-square fa-stack-2x grey"></i>
                  <i class="fa fa-ticket fa-stack-1x fa-inverse dark-blue"></i>
               </span>
                </a>
            </div>
            <div class="col-sm-6">
                <select class="js-select-options form-control" id="customerSelect">
                    @foreach(\App\Models\Customer::all() as $customer)
                        <option value="{{$customer->id}}">{{$customer->name}}</option>
                    @endforeach
                    {{--<option value="0">Walk in Customer</option>--}}
                    {{--<option value="7">Tiffany Green / +123 75674596</option>--}}
                    {{--<option value="8">Russell Thomas / +123 456789</option>--}}
                    {{--<option value="9">Gregory Hawkins / +123 456789</option>--}}
                    {{--<option value="10">Benjamin Smith / +123 456789</option>--}}
                </select>
                <span class="hidden" id="customerS"></span>
            </div>
            <div class="col-sm-6">
                <select class="js-select-options form-control" id="WaiterName">
                    {{--@foreach(\App\Models\Customer::all() as $customer)--}}
                        {{--<option value="{{$customer->id}}">{{$customer->name}}</option>--}}
                    {{--@endforeach--}}

                    <option value="0">without Waiter</option>
                    <option value="3">Eric Warren</option>
                    <option value="4">Carol Cooper</option>
                    <option value="5">Johnny Silva</option>
                </select>
                <span class="hidden" id="waiterS"></span>
            </div>
            <div class="col-sm-12">
                <form onsubmit="return barcode()">
                    <input type="text" autofocus id="" class="form-control barcode" placeholder="Barcode Scanner">
                </form>
            </div>
            <div class="col-xs-5 table-header">
                <h3>Product</h3>
            </div>
            <div class="col-xs-2 table-header">
                <h3>Price</h3>
            </div>
            <div class="col-xs-3 table-header nopadding">
                <h3 class="text-left">Quantity</h3>
            </div>
            <div class="col-xs-2 table-header nopadding">
                <h3>Total</h3>
            </div>
            <div id="productList">
                <!-- product List goes here  -->
            </div>

            <div class="footer-section">
                <div class="table-responsive col-sm-12 totalTab">
                    <table class="table">
                        <tr>
                            <td class="active" width="40%">SubTotal</td>
                            <td class="whiteBg" width="60%">
                                <span id="Subtot"></span> $
                                <span class="float-right"><b id="ItemsNum"><span></span> items</b></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="active">Order TAX</td>
                            <td class="whiteBg"><input type="text" value="10%" onchange="total_change()" id=""
                                                       class="total-input TAX" placeholder="N/A" maxlength="8">
                                <span class="float-right"><b id="taxValue"></b></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="active">Discount</td>
                            <td class="whiteBg"><input type="text" value="5%" onchange="total_change()" id=""
                                                       class="total-input Remise" placeholder="N/A" maxlength="8">
                                <span class="float-right"><b id="RemiseValue"></b></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="active">Total</td>
                            <td class="whiteBg light-blue text-bold"><span id="total"></span> $</td>
                        </tr>
                    </table>
                </div>
                <button type="button" onclick="cancelPOS()" class="btn btn-red col-md-6 flat-box-btn"><h5
                            class="text-bold">CANCEL</h5></button>
                <button type="button" class="btn btn-green col-md-6 flat-box-btn" data-toggle="modal"
                        data-target="#AddSale"><h5 class="text-bold ">PAYMENT</h5></button>
            </div>

        </div>
        <div class="col-md-7 right-side nopadding">
            <div class="row row-horizon">
                <span class="categories selectedGat" id=""><i class="fa fa-home"></i></span>
                @foreach(\App\Models\Category::all() as $category)
                    <span class="categories" id="cat-{{$category->id}}">{{$category->name}}</span>
                @endforeach
            </div>
            <div class="col-sm-12">
                <div id="searchContaner">
                    <div class="input-group stylish-input-group">
                        <input type="text" id="searchProd" class="form-control" placeholder="Search">
                        <span class="input-group-addon">
                              <button type="submit">
                                  <span class="glyphicon glyphicon-search"></span>
                              </button>
                          </span>
                    </div>
                </div>
            </div>
            <!-- product list section -->
            <div id="productList2">
                @foreach(\App\Models\Product::all() as $product)
                    <div class="col-sm-2 col-xs-4">
                        <a href="javascript:void(0)" class="addPct" id="product-{{$product->id}}"
                           onclick="add_posale('{{$product->id}}')">
                            <div class="product color03 flat-box">
                                {{--<h3 id="proname">Menu 02</h3>--}}
                                <input type="hidden" id="idname-{{$product->id}}" name="name" value="{{$product->name}}"/>
                                <input type="hidden" id="idprice-{{$product->id}}" name="price"
                                       value="{{$product->price}}"/>
                                <input type="hidden" id="category" name="category"
                                       value="cat-{{ $product->category_id }}"/>
                                <div class="mask">
                                    <h3>{{$product->price}}</h3>
                                    <p>
                                    <p>{{$product->name}}</p>
                                    </p>
                                </div>
                                <img src="{{ asset($product->image) }}" alt="Menu 02"></div>
                        </a>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
</div>

<!-- /.container -->
<script type="text/javascript">

    $(document).ready(function () {
        $('#productList').load("{{ url('pos/load_posales') }}");
        $('#Subtot').load("{{ url('pos/subtot') }}", null, total_change);
        $('#ItemsNum span, #ItemsNum2 span').load("{{ url('pos/totiems') }}");
        $('.holdList').load("{{ url('pos/holdList/74') }}", function () {
            var holdi = $('.selectedHold').attr("id");
            $('#waiterS').load("{{url("pos/WaiterName/")}}/" + holdi, function () {
                var res = $('#waiterS').text();
                if (res > 0) {
                    $('#WaiterName').val(res).trigger("change");
                } else {
                    $('#WaiterName').val(0).trigger("change");
                }
            });
            $('#customerS').load("{{ url('pos/CustomerName') }}/" + holdi, function () {
                var res = $('#customerS').text();
                if (res > 0) {
                    $('#customerSelect').val(res).trigger("change");
                } else {
                    $('#customerSelect').val(0).trigger("change");
                }
            });
        });

        $("#WaiterName").on('change', function () {
            var num = $('.selectedHold').attr("id");
            var id = $(this).val();
            $.ajax({
                url: "{{url("pos/changewaiterS/")}}",
                data: {num: num, id: id},
                type: "GET",
                success: function (data) {
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("error");
                }
            });
        });

        $("#customerSelect").on('change', function () {
            var num = $('.selectedHold').attr("id");
            var id = $(this).val();
            $.ajax({
                url: "{{url("pos/changecustomerS/")}}",
                data: {num: num, id: id},
                type: "GET",
                success: function (data) {
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("error");
                }
            });
        });


        $('.Paid').show();
        $('.ReturnChange').show();
        $('.CreditCardNum').hide();
        $('.CreditCardHold').hide();
        $('.ChequeNum').hide();
        $('.stripe-btn').hide();


        $("#paymentMethod").change(function () {

            var p_met = $(this).find('option:selected').val();

            if (p_met === '0') {
                $('.Paid').show();
                $('.ReturnChange').show();
                $('.CreditCardNum').hide();
                $('.CreditCardHold').hide();
                $('.CreditCardMonth').hide();
                $('.CreditCardYear').hide();
                $('.CreditCardCODECV').hide();
                $('#CreditCardNum').val('');
                $('#CreditCardHold').val('');
                $('#CreditCardYear').val('');
                $('#CreditCardMonth').val('');
                $('#CreditCardCODECV').val('');
                $('.stripe-btn').hide();
                $('.ChequeNum').hide();
            } else if (p_met === '1') {
                $('.Paid').show();
                $('.ReturnChange').hide();
                $('.CreditCardNum').show();
                $('.CreditCardHold').show();
                $('.CreditCardMonth').show();
                $('.CreditCardYear').show();
                $('.CreditCardCODECV').show();
                $('.stripe-btn').show();
                $('.ChequeNum').hide();
            } else if (p_met === '2') {
                $('.Paid').hide();
                $('.ReturnChange').hide();
                $('.CreditCardNum').hide();
                $('.CreditCardHold').hide();
                $('.CreditCardMonth').hide();
                $('.CreditCardYear').hide();
                $('.CreditCardCODECV').hide();
                $('#CreditCardNum').val('');
                $('#CreditCardHold').val('');
                $('#CreditCardYear').val('');
                $('#CreditCardMonth').val('');
                $('#CreditCardCODECV').val('');
                $('.stripe-btn').hide();
                $('.ChequeNum').show();
            }

        });
        /********************************* Credit Card infos section ****************************************/
        $('#CreditCardNum').validateCreditCard(function (result) {
            var cardtype = result.card_type == null ? '-' : result.card_type.name;
            $('.CreditCardNum i').removeClass('dark-blue');
            $('#' + cardtype).addClass('dark-blue');
        });

        $('#CreditCardNum').keypress(function (e) {
            var data = $(this).val();
            if (data.length > 22) {

                if (e.keyCode == 13) {
                    e.preventDefault();

                    var c = new SwipeParserObj(data);

                    $('#CreditCardNum').val(c.account);
                    $('#CreditCardHold').val(c.account_name);
                    $('#CreditCardYear').val(c.exp_year);
                    $('#CreditCardMonth').val(c.exp_month);
                    $('#CreditCardCODECV').val('');

                }
                else {
                    $('#CreditCardNum').val('');
                    $('#CreditCardHold').val('');
                    $('#CreditCardYear').val('');
                    $('#CreditCardMonth').val('');
                    $('#CreditCardCODECV').val('');
                }

                $('#CreditCardCODECV').focus();
                $('#CreditCardNum').validateCreditCard(function (result) {
                    var cardtype = result.card_type == null ? '-' : result.card_type.name;
                    $('.CreditCardNum i').removeClass('dark-blue');
                    $('#' + cardtype).addClass('dark-blue');
                });
            }

        });


        // ********************************* change calculations
        $('#Paid').on('keyup', function () {
            var change = -(parseFloat($('#total').text()) - parseFloat($(this).val()));
            if (change < 0) {
                $('#ReturnChange span').text(change.toFixed(2));
                $('#ReturnChange span').addClass("red");
                $('#ReturnChange span').removeClass("light-blue");
            } else {
                $('#ReturnChange span').text(change.toFixed(2));
                $('#ReturnChange span').removeClass("red");
                $('#ReturnChange span').addClass("light-blue");
            }
        });


        //  search product
        $("#searchProd").keyup(function () {
            // Retrieve the input field text
            var filter = $(this).val();

            // Loop through the list
            $("#productList2 #proname").each(function () {
                // If the list item does not contain the text phrase fade it out
                if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                    $(this).parent().parent().parent().hide();
                    // Show the list item if the phrase matches
                } else {
                    $(this).parent().parent().parent().show();
                }
            });
        });
    });

    // barcode scanner
    function barcode() {
        var code = $('.barcode').val();
        $.ajax({
            url: "{{ $rest_url }}/zarest/pos/findproduct/" + code,
            type: "POST",
            dataType: "JSON",
            success: function (data) {
                add_posale(data);
                $('.barcode').val('');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("error");
            }
        });
        return false;
    };

    //  **********************select categorie

    $(".categories").on("click", function () {
        // Retrieve the input field text
        var filter = $(this).attr('id');
        $(this).parent().children().removeClass('selectedGat');

        $(this).addClass('selectedGat');
        // Loop through the list
        $("#productList2 #category").each(function () {
            // If the list item does not contain the text phrase fade it out
            if ($(this).val().search(new RegExp(filter, "i")) < 0) {
                $(this).parent().parent().parent().hide();
                // Show the list item if the phrase matches
            } else {
                $(this).parent().parent().parent().show();
            }
        });
    });

    // function to calculate a percentage from a number
    function percentage(tot, n) {
        var perc;
        perc = ((parseFloat(tot) * (parseFloat(n ? n : 0) * 0.01)));
        return perc;
    }

    // function to calculate the total number
    function total_change() {
        var tot;
        if (($('.TAX').val().indexOf('%') == -1) && ($('.Remise').val().indexOf('%') == -1)) {
            tot = parseFloat($('#Subtot').text().replace(/ /g, '')) + parseFloat($('.TAX').val() ? $('.TAX').val() : 0);
            $('#taxValue').text('$');
            $('#RemiseValue').text('$');
            tot = tot - parseFloat($('.Remise').val() ? $('.Remise').val() : 0);
            $('#total').text(tot.toFixed(2));
            $('#Paid').val(tot.toFixed(2));
            $('#TotalModal').text('Total ' + tot.toFixed(2) + '$');
        } else if (($('.TAX').val().indexOf('%') != -1) && ($('.Remise').val().indexOf('%') == -1)) {
            tot = parseFloat($('#Subtot').text()) + percentage($('#Subtot').text(), $('.TAX').val());
            $('#taxValue').text(percentage($('#Subtot').text(), $('.TAX').val()).toFixed(2) + ' $');
            $('#RemiseValue').text('$');
            tot = tot - parseFloat($('.Remise').val() ? $('.Remise').val() : 0);
            $('#total').text(tot.toFixed(2));
            $('#Paid').val(tot.toFixed(2));
            $('#TotalModal').text('Total ' + tot.toFixed(2) + ' $');
        } else if (($('.TAX').val().indexOf('%') != -1) && ($('.Remise').val().indexOf('%') != -1)) {
            tot = parseFloat($('#Subtot').text()) + percentage($('#Subtot').text(), $('.TAX').val());
            $('#taxValue').text(percentage($('#Subtot').text(), $('.TAX').val()).toFixed(2) + ' $');
            tot = tot - percentage($('#Subtot').text(), $('.Remise').val());
            $('#RemiseValue').text(percentage($('#Subtot').text(), $('.Remise').val()).toFixed(2) + ' $');
            $('#total').text(tot.toFixed(2));
            $('#Paid').val(tot.toFixed(2));
            $('#TotalModal').text('Total ' + tot.toFixed(2) + ' $');
        } else if (($('.TAX').val().indexOf('%') == -1) && ($('.Remise').val().indexOf('%') != -1)) {
            tot = parseFloat($('#Subtot').text()) + parseFloat($('.TAX').val() ? $('.TAX').val() : 0);
            tot = tot - percentage($('#Subtot').text(), $('.Remise').val());
            $('#taxValue').text('$');
            $('#RemiseValue').text(percentage($('#Subtot').text(), $('.Remise').val()).toFixed(2) + ' $');
            $('#total').text(tot.toFixed(2));
            $('#Paid').val(tot.toFixed(2));
            $('#TotalModal').text('Total ' + tot.toFixed(2) + ' $');
        }
    }


    function delete_posale(id) {
        // ajax delete data to database
        $.ajax({
            url: "{{ url('pos/delete/')}}/" + id,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                $('#productList').load("{{ url('pos/load_posales') }}");
                $('#ItemsNum span, #ItemsNum2 span').load("{{ url('pos/totiems') }}");
                $('#Subtot').load("{{ url('pos/subtot') }}", null, total_change);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#productList').load("{{ url('pos/load_posales') }}");
                $('#ItemsNum span, #ItemsNum2 span').load("{{ url('pos/totiems') }}");
                $('#Subtot').load("{{ url('pos/subtot') }}", null, total_change);
            }
        });

    }

    /********************************** Hold functions ************************************/
    function AddHold() {
        $.ajax({
            url: "{{ url('pos/AddHold/74') }}",
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                $('#productList').load("{{ url('pos/load_posales') }}");
                $('#ItemsNum span, #ItemsNum2 span').load("{{ url('pos/totiems') }}");
                $('#Subtot').load("{{ url('pos/subtot') }}", null, total_change);
                $('.holdList').load("{{ url('pos/holdList/74') }}");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("error");
            }
        });

    }

    function RemoveHold() {
        var number = $('.selectedHold').clone().children().remove().end().text();
        if (number != 1) {
            swal({
                    title: 'Are you sure ?',
                    text: 'You will not be able to recover this Data later!',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: 'Yes, delete it!',
                    closeOnConfirm: false
                },
                function () {
                    // ajax delete data to database
                    $.ajax({
                        url: "{{ url('pos/RemoveHold/')}}/" + number + "/65",
                        type: "GET",
                        dataType: "JSON",
                        success: function (data) {
                            $('#productList').load("{{ url('pos/load_posales') }}");
                            $('#ItemsNum span, #ItemsNum2 span').load("{{ url('pos/totiems') }}");
                            $('#Subtot').load("{{ url('pos/subtot') }}", null, total_change);
                            $('.holdList').load("{{ url('pos/holdList/74') }}");
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("error");
                        }
                    });
                    swal.close();
                });
        }

    }

    function SelectHold(number) {
        // ajax delete data to database
        $.ajax({
            url: "{{url("pos/SelectHold/")}}/" + number,
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                $('#productList').load("{{ url('pos/load_posales') }}");
                $('#ItemsNum span, #ItemsNum2 span').load("{{ url('pos/totiems') }}");
                $('#Subtot').load("{{ url('pos/subtot') }}", null, total_change);
                $('#' + number).parent().children().removeClass('selectedHold');
                $('#' + number).addClass('selectedHold');
                $('#waiterS').load("{{url("pos/WaiterName/")}}/" + number, function () {
                    var res = $('#waiterS').text();
                    if (res > 0) {
                        $('#WaiterName').val(res).trigger("change");
                    } else {
                        $('#WaiterName').val(0).trigger("change");
                    }
                });
                $('#customerS').load("{{url("pos/CustomerName/")}}/" + number, function () {
                    var res = $('#customerS').text();
                    if (res > 0) {
                        $('#customerSelect').val(res).trigger("change");
                    } else {
                        $('#customerSelect').val(0).trigger("change");
                    }
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("error");
            }
        });

    }

    /********************************** end Hold functions ************************************/

    function add_posale(id) {
        var name1 = $('#idname-' + id).val();
        var price1 = $('#idprice-' + id).val();
        //var number = $('.selectedHold').clone().children().remove().end().text();
        var number = $('.selectedHold').prop('id');
        var waiterID = $('#WaiterName').find('option:selected').val();
        // ajax delete data to database
        $.ajax({
            url: "{{ url('pos/addpdc') }}",
            type: "GET",
            data: {name: name1, price: price1, product_id: id, number: number, registerid: 65, waiter: waiterID},
            success: function (data) {
                if (data === 'stock') {
                    swal("Low inventory");
                } else {
                    $('#productList').load("{{ url('pos/load_posales') }}");
                    $('#ItemsNum span, #ItemsNum2 span').load("{{ url('pos/totiems') }}");
                    $('#Subtot').load("{{ url('pos/subtot') }}", null, total_change);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("error");
            }
        });

    }


    function addoptions(id, posale) {
        $('#optionsSection').load("{{ $rest_url }}/zarest/pos/getoptions/" + id + "/" + posale, function () {
            $(".js-select-basic-multiple").select2();
        });
        $('#options').modal('show');
    }

    function addPoptions() {
        var options = $('#optionsselect').val();
        var posale = $('#optprd').val();
        $.ajax({
            url: "{{ $rest_url }}/zarest/pos/addposaleoptions",
            type: "POST",
            data: {options: options, posale: posale},
            success: function (data) {
                $('#options').modal('hide');
                $('#pooptions-' + posale).text(options);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("error");
            }
        });
    }

    function edit_posale(id) {
        var qt1 = $('#qt-' + id).val();
        $.ajax({
            url: "{{ url('pos/edit/')}}/" + id,
            type: "GET",
            data: {qt: qt1},
            success: function (data) {
                if (data === 'stock') {
                    swal("Low inventory");
                    $('#productList').load("{{ url('pos/load_posales') }}");
                } else {
                    $('#productList').load("{{ url('pos/load_posales') }}");
                    $('#ItemsNum span, #ItemsNum2 span').load("{{ url('pos/totiems') }}");
                    $('#Subtot').load("{{ url('pos/subtot') }}", null, total_change);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("error");
            }
        });

    }


    $("#customerSelect").change(function () {

        var id = $(this).find('option:selected').val();
        console.log('id');
        if (id === '0') {
            $('.Remise').val('5%');
        } else {
            $.ajax({
                url: "{{ url('pos/GetDiscount/')}}/" + id,
                type: "GET",
                success: function (data) {
                    var values = data.split('~');
                    $('#customerName span').text(values[1]);
                    $('.Remise').val(values[0]);
                    $('#Subtot').load("{{ url('pos/subtot') }}", null, total_change);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("error");
                }
            });
        }
    });

    function cancelPOS() {
        swal({
                title: 'Are you sure ?',
                text: 'You will not be able to recover this Data later!',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false
            },
            function () {
                $('#customerSelect').val('0');
                $('#customerSelect').trigger('change.select2');
                $('.Remise').val('5%');
                $('.TAX').val('10%');

                $.ajax({
                    url: "{{ url('pos/ResetPos/') }}",
                    type: "GET",
                    success: function (data) {
                        $('#productList').load("{{ url('pos/load_posales') }}");
                        $('#Subtot').load("{{ url('pos/subtot') }}", null, total_change);
                        $('#ItemsNum span, #ItemsNum2 span').text("0");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("error");
                    }
                });
                swal('Deleted!', 'the data has been deleted.', "success");
            });
    }


    function saleBtn(type) {

        var clientID = $('#customerSelect').find('option:selected').val();
        var clientName = $('#customerName span').text();
        var Tax = $('.TAX').val();
        var Discount = $('.Remise').val();
        var Subtotal = $('#Subtot').text();
        var Total = $('#total').text();
        var createdBy = 'sale Staff';
        var totalItems = $('#ItemsNum span').text();
        var Paid = $('#Paid').val();
        var paidMethod = $('#paymentMethod').find('option:selected').val();
        var Status = 0;
        var ccnum = $('#CreditCardNum').val();
        var ccmonth = $('#CreditCardMonth').val();
        var ccyear = $('#CreditCardYear').val();
        var ccv = $('#CreditCardCODECV').val();
        var waiter = $('#WaiterName').val();
        switch (paidMethod) {
            case '1':
                paidMethod += '~' + $('#CreditCardNum').val() + '~' + $('#CreditCardHold').val();
                break;
            case '2':
                paidMethod += '~' + $('#ChequeNum').val()
                break;
            case '0':
                var change = parseFloat(Total) - parseFloat(Paid);
                if (change == parseFloat(Total)) Status = 1;
                else if (change > 0) Status = 2;
                else if (change <= 0) Status = 0;
        }
        var taxamount = $('.TAX').val().indexOf('%') != -1 ? parseFloat($('#taxValue').text()) : $('.TAX').val();
        var discountamount = $('.Remise').val().indexOf('%') != -1 ? parseFloat($('#RemiseValue').text()) : $('.Remise').val();

        $.ajax({
            url: "{{ url('pos/AddNewSale/')}}/" + type,
            type: "GET",
            data: {
                client_id: clientID,
                clientname: clientName,
                waiter_id: waiter,
                discountamount: discountamount,
                taxamount: taxamount,
                tax: Tax,
                discount: Discount,
                subtotal: Subtotal,
                total: Total,
                created_by: createdBy,
                totalitems: totalItems,
                paid: Paid,
                status: Status,
                paidmethod: paidMethod,
                ccnum: ccnum,
                ccmonth: ccmonth,
                ccyear: ccyear,
                ccv: ccv
            },
            success: function (data) {
                console.log(data);
                $('#printSection').html(data);
                $('#productList').load("{{ url('pos/load_posales') }}");
                $('#ItemsNum span, #ItemsNum2 span').load("{{ url('pos/totiems') }}");
                $('#Subtot').load("{{ url('pos/subtot') }}", null, total_change);
                $('#AddSale').modal('hide');
                $('#ticket').modal('show');
                $('#ReturnChange span').text('0');
                $('#Paid').val('0');
                $('.holdList').load("{{ url('pos/holdList/74') }}");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("error");
            }
        });

        $('#CreditCardNum').val('');
        $('#CreditCardHold').val('');
        $('#CreditCardYear').val('');
        $('#CreditCardMonth').val('');
        $('#CreditCardCODECV').val('');

    }

    function PrintTicket() {
        $('.modal-body').removeAttr('id');
        window.print();
        $('.modal-body').attr('id', 'modal-body');
    }


    function email() {
        $('#ticket').modal('hide');
        swal({
                title: "An input!",
                text: "Email:",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "Email"
            },
            function (inputValue) {
                if (inputValue === false) return false;
                if (inputValue === "") {
                    swal.showInputError("You need to write an email!");
                    return false
                }
                var content = $('#printSection').html();
                $.ajax({
                    url: "{{ $rest_url }}/zarest/pos/email/",
                    type: "POST",
                    data: {content: content, email: inputValue},
                    success: function (data) {
                        $('#ticket').modal('show');
                        swal.close();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("error");
                    }
                });
            });
    }

    function pdfreceipt() {
        var content = $('#printSection').html();
        $.redirect('Receipt', {content: content});
    }

    function showticket() {
        var hold = $('.selectedHold').attr("id");
        var total = encodeURIComponent($('#total').text());
        var taxValue = encodeURIComponent($('#taxValue').text());
        var RemiseValue = encodeURIComponent( $('#RemiseValue').text());
        //alert(RemiseValue);
        $('#printSection').load("{{url("pos/showticket/")}}/" + hold + "/" + total + "/" + taxValue + "/" + RemiseValue);
        $('#ticket').modal('show');
    }

</script>


<!-- Modal -->
<div class="modal fade" id="AddSale" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="AddSale">Add Sale</h4>
            </div>
            <form>
                <div class="modal-body">
                    <div class="form-group">
                        <h2 id="customerName">Customer <span>Walk in Customer</span></h2>
                    </div>
                    <div class="form-group">
                        <h3 id="ItemsNum2"><span></span> items</h3>
                    </div>
                    <div class="form-group">
                        <h2 id="TotalModal"></h2>
                    </div>
                    <div class="form-group">
                        <label for="paymentMethod">Payment method:</label>
                        <select class="js-select-options form-control" id="paymentMethod">
                            <option value="0">Cash</option>
                            <option value="1">Credit Card</option>
                            <option value="2">Cheque</option>
                        </select>
                    </div>
                    <div class="form-group Paid">
                        <label for="Paid">Paid</label>
                        <input type="text" value="0" name="paid" class="form-control " id="Paid" placeholder="Paid">
                    </div>
                    <div class="form-group CreditCardNum">
                        <i class="fa fa-cc-visa fa-2x" id="visa" aria-hidden="true"></i>
                        <i class="fa fa-cc-mastercard fa-2x" id="mastercard" aria-hidden="true"></i>
                        <i class="fa fa-cc-amex fa-2x" id="amex" aria-hidden="true"></i>
                        <i class="fa fa-cc-discover fa-2x" id="discover" aria-hidden="true"></i>
                        <label for="CreditCardNum">Credit Card Number (swipe)</label>
                        <input type="text" class="form-control cc-num" id="CreditCardNum"
                               placeholder="Credit Card Number (swipe)">
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group CreditCardHold col-md-4 padding-s">
                        <input type="text" class="form-control" id="CreditCardHold" placeholder="Credit Card Holder">
                    </div>
                    <div class="form-group CreditCardHold col-md-2 padding-s">
                        <input type="text" class="form-control" id="CreditCardMonth" placeholder="Month">
                    </div>
                    <div class="form-group CreditCardHold col-md-2 padding-s">
                        <input type="text" class="form-control" id="CreditCardYear" placeholder="Year">
                    </div>
                    <div class="form-group CreditCardHold col-md-4 padding-s">
                        <input type="text" class="form-control" id="CreditCardCODECV" placeholder="CODE CV">
                    </div>
                    <div class="form-group ChequeNum">
                        <label for="ChequeNum">Cheque Number</label>
                        <input type="text" name="chequenum" class="form-control" id="ChequeNum"
                               placeholder="Cheque Number">
                    </div>
                    <div class="form-group ReturnChange">
                        <h3 id="ReturnChange">Change <span>0</span> $</h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-add stripe-btn" onclick="saleBtn(2)"><i class="fa fa-cc-stripe"
                                                                                                 aria-hidden="true"></i>
                        StripePayment
                    </button>
                    <button type="button" class="btn btn-add submit_total" id="submit_total" onclick="saleBtn(1)">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->


<!-- Modal ticket -->
<div class="modal fade" id="ticket" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" id="ticketModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="ticket">Receipt</h4>
            </div>
            <div class="modal-body" id="modal-body">
                <div id="printSection">
                    <!-- Ticket goes here -->
                    <center><h1 style="color:#34495E">Empty</h1></center>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default hiddenpr" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-add hiddenpr" href="javascript:void(0)" onClick="pdfreceipt()">
                    PDF
                </button>
                <button type="button" class="btn btn-add hiddenpr" onclick="email()">email</button>
                <button type="button" class="btn btn-add hiddenpr" onclick="PrintTicket()">print</button>
            </div>
        </div>
    </div>
</div>
<!-- /.Modal -->

<!-- Modal options -->
<div class="modal fade" id="options" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" id="ticketModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="ticket">Options</h4>
            </div>
            <div class="modal-body" id="modal-body">
                <div id="optionsSection">
                    <!-- Ticket goes here -->
                    <center><h1 style="color:#34495E">Empty</h1></center>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default hiddenpr" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-add" onclick="addPoptions()">Submit</button>
            </div>
        </div>
    </div>
</div>
<!-- /.Modal -->

<!-- Modal add user -->
<div class="modal fade" id="AddCustomer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add Customer</h4>
            </div>
            <form action="{{ url('admin/customer/') }}" method="post" accept-charset="utf-8"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="CustomerName">Customer Name</label>
                        <input type="text" name="name" class="form-control" id="CustomerName"
                               placeholder="Customer Name">
                    </div>
                    <div class="form-group">
                        <label for="CustomerPhone">Phone</label>
                        <input type="text" name="phone" class="form-control" id="CustomerPhone" placeholder="Phone">
                    </div>
                    <div class="form-group">
                        <label for="CustomerEmail">Email</label>
                        <input type="email" name="email" class="form-control" id="CustomerEmail" placeholder="Email">
                    </div>
                    {{--<div class="form-group">--}}
                    {{--<label for="CustomerDiscount">Discount</label>--}}
                    {{--<input type="text" name="discount" class="form-control" id="CustomerDiscount"--}}
                    {{--placeholder="Discount">--}}
                    {{--</div>--}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-add ">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /.Modal -->


<script type="text/javascript">
    function CloseRegister() {
        $.ajax({
            url: "{{ $rest_url }}/zarest/pos/CloseRegister/",
            type: "POST",
            success: function (data) {
                $('#closeregsection').html(data);
                $('#CloseRegister').modal('show');
                setTimeout(function () {
                    $('#countedcash').focus()
                }, 1000);
                $('#countedcash').on('keyup', function () {
                    var change = -(parseFloat($('#expectedcash').text()) - parseFloat($(this).val()));
                    var difftot = change + parseFloat($('#diffcc').text()) + parseFloat($('#diffcheque').text());
                    var total = parseFloat($('#countedcc').val()) + parseFloat($('#countedcheque').val()) + parseFloat($('#countedcash').val());
                    $('#countedtotal').text(total.toFixed(2));
                    $('#difftotal').text(difftot.toFixed(2))
                    if (change < 0) {
                        $('#diffcash').text(change.toFixed(2));
                        $('#diffcash').addClass("red");
                        $('#diffcash').removeClass("light-blue");
                    } else {
                        $('#diffcash').text(change.toFixed(2));
                        $('#diffcash').removeClass("red");
                        $('#diffcash').addClass("light-blue");
                    }
                });

                $('#countedcc').on('keyup', function () {
                    var change = -(parseFloat($('#expectedcc').text()) - parseFloat($(this).val()));
                    var difftot = change + parseFloat($('#diffcash').text()) + parseFloat($('#diffcheque').text());
                    var total = parseFloat($('#countedcc').val()) + parseFloat($('#countedcheque').val()) + parseFloat($('#countedcash').val());
                    $('#countedtotal').text(total.toFixed(2));
                    $('#difftotal').text(difftot.toFixed(2))
                    if (change < 0) {
                        $('#diffcc').text(change.toFixed(2));
                        $('#diffcc').addClass("red");
                        $('#diffcc').removeClass("light-blue");
                    } else {
                        $('#diffcc').text(change.toFixed(2));
                        $('#diffcc').removeClass("red");
                        $('#diffcc').addClass("light-blue");
                    }
                });

                $('#countedcheque').on('keyup', function () {
                    var change = -(parseFloat($('#expectedcheque').text()) - parseFloat($(this).val()));
                    var difftot = change + parseFloat($('#diffcc').text()) + parseFloat($('#diffcash').text());
                    var total = parseFloat($('#countedcc').val()) + parseFloat($('#countedcheque').val()) + parseFloat($('#countedcash').val());
                    $('#countedtotal').text(total.toFixed(2));
                    $('#difftotal').text(difftot.toFixed(2))
                    if (change < 0) {
                        $('#diffcheque').text(change.toFixed(2));
                        $('#diffcheque').addClass("red");
                        $('#diffcheque').removeClass("light-blue");
                    } else {
                        $('#diffcheque').text(change.toFixed(2));
                        $('#diffcheque').removeClass("red");
                        $('#diffcheque').addClass("light-blue");
                    }
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert("error");
            }
        });
    }

    function SubmitRegister() {
        var expectedcash = $('#expectedcash').text();
        var countedcash = $('#countedcash').val();
        var expectedcc = $('#expectedcc').text();
        var countedcc = $('#countedcc').val();
        var expectedcheque = $('#expectedcheque').text();
        var countedcheque = $('#countedcheque').val();
        var RegisterNote = $('#RegisterNote').val();

        swal({
                title: 'Are you sure ?',
                text: 'You will not be able to recover the Holds later!',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Yes, Close it!',
                closeOnConfirm: false
            },
            function () {

                $.ajax({
                    url: "{{ $rest_url }}/zarest/pos/SubmitRegister/",
                    type: "POST",
                    data: {
                        expectedcash: expectedcash,
                        countedcash: countedcash,
                        expectedcc: expectedcc,
                        countedcc: countedcc,
                        expectedcheque: expectedcheque,
                        countedcheque: countedcheque,
                        RegisterNote: RegisterNote
                    },
                    success: function (data) {
                        window.location.href = "{{ $rest_url }}/zarest/";
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("error");
                    }
                });

                swal.close();
            });
    }

    function CloseTable() {

        swal({
                title: 'Are you sure ?',
                text: 'You will not be able to recover the Holds later!',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Yes, Close it!',
                closeOnConfirm: false
            },
            function () {

                $.ajax({
                    url: "{{ $rest_url }}/zarest/pos/CloseTable/",
                    type: "POST",
                    success: function (data) {
                        window.location.href = "{{ $rest_url }}/zarest/";
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("error");
                    }
                });

                swal.close();
            });
    }


</script>
<!-- Modal close register -->
<div class="modal fade" id="CloseRegister" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">close&nbsp;register</h4>
            </div>
            <div class="modal-body">
                <div id="closeregsection">
                    <!-- close register detail goes here -->
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" onclick="SubmitRegister()" class="btn btn-red col-md-12 flat-box-btn">close&nbsp;register</a>
            </div>
        </div>
    </div>
</div>
<!-- /.Modal -->


<!-- slim scroll script -->
<script type="text/javascript" src="{{ $rest_url }}/zarest/assets/js/jquery.slimscroll.min.js"></script>
<!-- waves material design effect -->
<script type="text/javascript" src="{{ $rest_url }}/zarest/assets/js/waves.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script type="text/javascript" src="{{ $rest_url }}/zarest/assets/js/bootstrap.min.js"></script>
<!-- keyboard widget dependencies -->
<script type="text/javascript" src="{{ $rest_url }}/zarest/assets/js/jquery.keyboard.js"></script>
<script type="text/javascript" src="{{ $rest_url }}/zarest/assets/js/jquery.keyboard.extension-all.js"></script>
<script type="text/javascript" src="{{ $rest_url }}/zarest/assets/js/jquery.keyboard.extension-extender.js"></script>
<script type="text/javascript" src="{{ $rest_url }}/zarest/assets/js/jquery.keyboard.extension-typing.js"></script>
<script type="text/javascript" src="{{ $rest_url }}/zarest/assets/js/jquery.mousewheel.js"></script>
<!-- select2 plugin script -->
<script type="text/javascript" src="{{ $rest_url }}/zarest/assets/js/select2.min.js"></script>
<!-- dalatable scripts -->
<script src="{{ $rest_url }}/zarest/assets/datatables/js/jquery.dataTables.min.js"></script>
<script src="{{ $rest_url }}/zarest/assets/datatables/js/dataTables.bootstrap.js"></script>
<!-- summernote js -->
<script src="{{ $rest_url }}/zarest/assets/js/summernote.js"></script>
<!-- chart.js script -->
<script src="{{ $rest_url }}/zarest/assets/js/Chart.js"></script>
<!-- moment JS -->
<script type="text/javascript" src="{{ $rest_url }}/zarest/assets/js/moment.min.js"></script>
<!-- Include Date Range Picker -->
<script type="text/javascript" src="{{ $rest_url }}/zarest/assets/js/daterangepicker.js"></script>
<!-- Sweet Alert swal -->
<script src="{{ $rest_url }}/zarest/assets/js/sweetalert.min.js"></script>
<!-- datepicker script -->
<script src="{{ $rest_url }}/zarest/assets/js/bootstrap-datepicker.min.js"></script>
<!-- creditCardValidator script -->
<script src="{{ $rest_url }}/zarest/assets/js/jquery.creditCardValidator.js"></script>
<!-- creditCardValidator script -->
<script src="{{ $rest_url }}/zarest/assets/js/credit-card-scanner.js"></script>
<script src="{{ $rest_url }}/zarest/assets/js/jquery.redirect.js"></script>
<!-- ajax form -->
<script src="{{ $rest_url }}/zarest/assets/js/jquery.form.min.js"></script>
<!-- custom script -->
<script src="{{ $rest_url }}/zarest/assets/js/app.js"></script>
{{--<script>--}}
    {{--$(document).ready(function(){--}}
        {{--$("#submit_total").on('click', function () {--}}
            {{--var total = $('#total').val();--}}
            {{--alert(total);--}}
        {{--});--}}
    {{--});--}}
{{--</script>--}}
</body>
</html>
