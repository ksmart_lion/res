
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>POS - point of sale Dar Elweb</title>
    <!-- jQuery -->
    <script type="text/javascript" src="{{ $rest_url }}/zarest/assets/js/jquery-2.2.2.min.js"></script>
    <script type="text/javascript" src="{{ $rest_url }}/zarest/assets/js/loading.js"></script>
    <!-- normalize & reset style -->
    <link rel="stylesheet" href="{{ $rest_url }}/zarest/assets/css/normalize.min.css" type='text/css'>
    <link rel="stylesheet" href="{{ $rest_url }}/zarest/assets/css/reset.min.css" type='text/css'>
    <link rel="stylesheet" href="{{ $rest_url }}/zarest/assets/css/jquery-ui.css" type='text/css'>
    <!-- google lato/Kaushan/Pinyon fonts -->
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,300' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pinyon+Script" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link href="{{ $rest_url }}/zarest/assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap-horizon -->
    <link href="{{ $rest_url }}/zarest/assets/css/bootstrap-horizon.css" rel="stylesheet">
    <!-- datatable style -->
    <link href="{{ $rest_url }}/zarest/assets/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
    <!-- font awesome -->
    <link rel="stylesheet" href="{{ $rest_url }}/zarest/assets/css/font-awesome.min.css">
    <!-- include summernote css-->
    <link href="{{ $rest_url }}/zarest/assets/css/summernote.css" rel="stylesheet">
    <!-- waves -->
    <link rel="stylesheet" href="{{ $rest_url }}/zarest/assets/css/waves.min.css">
    <!-- daterangepicker -->
    <link rel="stylesheet" type="text/css" href="{{ $rest_url }}/zarest/assets/css/daterangepicker.css"/>
    <!-- css for the preview keyset extension -->
    <link href="{{ $rest_url }}/zarest/assets/css/keyboard-previewkeyset.css" rel="stylesheet">
    <!-- keyboard widget style -->
    <link href="{{ $rest_url }}/zarest/assets/css/keyboard.css" rel="stylesheet">
    <!-- Select 2 style -->
    <link href="{{ $rest_url }}/zarest/assets/css/select2.min.css" rel="stylesheet">
    <!-- Sweet alert swal -->
    <link rel="stylesheet" type="text/css" href="{{ $rest_url }}/zarest/assets/css/sweetalert.css">
    <!-- datepicker css -->
    <link rel="stylesheet" type="text/css" href="{{ $rest_url }}/zarest/assets/css/bootstrap-datepicker.min.css">
    <!-- Custom CSS -->
    <link href="{{ $rest_url }}/zarest/assets/css/Style-Light.css" rel="stylesheet">
    <!-- favicon -->
    <link rel="shortcut icon" href="{{ $rest_url }}/zarest//favicon.ico?v=2" type="image/x-icon">
    <link rel="icon" href="{{ $rest_url }}/zarest//favicon.ico?v=2" type="image/x-icon">
</head>
<body>
<!-- Navigation -->
<!-- Page Content -->

<div class="container">
    <?php
        $total=0
        ?>
    @foreach(\App\Models\Sall::whereDate('created_at',\Carbon\Carbon::today())->get() as $sale )
       $total +=  $sale->total;
    @endforeach

    <div class="row" style="margin-top:10px;">
        <div class="col-md-4">
            <div class="statCart Statcolor01">
                <i class="fa fa-users" aria-hidden="true"></i>
                <h1 class="count">{{
                    \App\Models\Customer::count()
                    }} </h1><br>
                <span>Customers</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="statCart Statcolor02">
                <i class="fa fa-archive" aria-hidden="true"></i>
                <h1 class="count">{{
                    \App\Models\Product::count()
                    }} </h1><br>
                <span>Product (in
                    {{
                    \App\Models\Category::count()
                    }} Categories)</span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="statCart Statcolor03">
                <i class="fa fa-money" aria-hidden="true"></i>
                <h2 style="display: inline"><span class="count">
                        <?php echo $total ?>
                    </span> USD</h2><br>
                <span>Today's Sale</span>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top:50px;">
        <div class="col-md-8">
            <!-- chart container  -->
            <div class="statCart">
                <h3>monthly Stats</h3>
                <div style="width:100%">
                    <canvas id="canvas" height="330" width="750"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <!-- pie container  -->
            <div class="statCart">
                <h3>Top 5 Products this year</h3>
                <div id="canvas-holder">
                    <canvas id="chart-area2" width="230" height="230" />  </div>
            </div>
        </div>
    </div>
    {{--<div class="row" style="margin-top: 50px;">--}}
        {{--<div class="col-md-12">--}}
            {{--<div class="statCart">--}}
                {{--<div class="col-md-2"><h4><center>Top 5 Products this year</center></h4></div>--}}
                {{--<div class="col-md-2"><h2><center><span class="label label-default" style="background-color: #F3565D;">Menu 02</span></center></h2></div>--}}
                {{--<div class="col-md-2"><h2><center><span class="label label-default" style="background-color: #FC9D9B;">tomato salad</span></center></h2></div>--}}
                {{--<div class="col-md-2"><h2><center><span class="label label-default" style="background-color: #FACDAE;">Salad</span></center></h2></div>--}}
                {{--<div class="col-md-2"><h2><center><span class="label label-default" style="background-color: #9FC2C4;">Bana juice</span></center></h2></div>--}}
                {{--<div class="col-md-2"><h2><center><span class="label label-default" style="background-color: #8297A8;">Potato Salad</span></center></h2></div>--}}
                {{--<div style="clear:both;"></div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <!-- ************************************************************************************************** -->

    <div class="row rangeStat" style="margin-top:50px; margin-bottom:70px;">
        <div class="col-md-12">
            <div class="statCart">
                <h1 class="statYear">2018</h1>
                <button class="btn btn-Year" type="button" onclick="getyearstats('next')"><</button>
                <button class="btn btn-Year" type="button" onclick="getyearstats('prev')">></button>
                <div class="float-right" style="margin-top: 50px;">
                    <span class="revenuespan" style="font-size:11px;">Revenue</span>
                    <span class="expencespan" style="font-size:11px;">Expense</span>
                </div>
                <div id="statyears">
                    <table class="StatTable">
                        <tr>
                            <td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>tax : <b>0 USD</b> <br><br> Discount : <b>0 USD</b></h5>">0 USD</span><span class="expencespan"> USD</span>January</td>
                            <td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>tax : <b>0 USD</b> <br><br> Discount : <b>0 USD</b></h5>">0 USD</span><span class="expencespan"> USD</span>February</td>
                            <td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>tax : <b>0 USD</b> <br><br> Discount : <b>0 USD</b></h5>">0 USD</span><span class="expencespan"> USD</span>March</td>
                            <td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>tax : <b>0 USD</b> <br><br> Discount : <b>0 USD</b></h5>">0 USD</span><span class="expencespan"> USD</span>April</td>
                        </tr>
                        <tr>
                            <td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>tax : <b>0 USD</b> <br><br> Discount : <b>0 USD</b></h5>">0 USD</span><span class="expencespan"> USD</span>May</td>
                            <td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>tax : <b>0 USD</b> <br><br> Discount : <b>0 USD</b></h5>">0 USD</span><span class="expencespan"> USD</span>June</td>
                            <td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>tax : <b>63 USD</b> <br><br> Discount : <b>74 USD</b></h5>">1100 USD</span><span class="expencespan"> USD</span>July</td>
                            <td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>tax : <b>0 USD</b> <br><br> Discount : <b>0 USD</b></h5>">0 USD</span><span class="expencespan"> USD</span>August</td>
                        </tr>
                        <tr>
                            <td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>tax : <b>0 USD</b> <br><br> Discount : <b>0 USD</b></h5>">0 USD</span><span class="expencespan"> USD</span>September</td>
                            <td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>tax : <b>0 USD</b> <br><br> Discount : <b>0 USD</b></h5>">0 USD</span><span class="expencespan"> USD</span>October</td>
                            <td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>tax : <b>0 USD</b> <br><br> Discount : <b>0 USD</b></h5>">0 USD</span><span class="expencespan"> USD</span>November</td>
                            <td><span class="revenuespan" data-toggle="tooltip" data-placement="top"  data-html="true" title="<h5>tax : <b>0 USD</b> <br><br> Discount : <b>0 USD</b></h5>">0 USD</span><span class="expencespan"> USD</span>December</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{--<div class="row rangeStat" style="margin-top:50px;">--}}
        {{--<h3 class="col-sm-12">Clients Stats</h3>--}}
        {{--<div class="col-md-5">--}}
            {{--<div class="form-group">--}}
                {{--<label for="customerSelect">Select Client</label>--}}
                {{--<select class="js-select-options form-control" id="customerSelect">--}}
                    {{--<option value="0">Walk in Customer</option>--}}
                    {{--<option value="7">Tiffany Green</option>--}}
                    {{--<option value="8">Russell Thomas</option>--}}
                    {{--<option value="9">Gregory Hawkins</option>--}}
                    {{--<option value="10">Benjamin Smith</option>--}}
                {{--</select>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-5">--}}
            {{--<div class="form-group">--}}
                {{--<label>Select Range</label>--}}
                {{--<div class="input-group margin-bottom-sm">--}}
                    {{--<span class="input-group-addon RangePicker"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>--}}
                    {{--<input class="form-control" id="CustomerRange" type="text" name="daterange" />--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-2">--}}
            {{--<button class="cancelBtn btn btn-picker" type="button" onclick="getCustomerReport()">Get Report</button>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="row rangeStat" style="margin-top:50px;">--}}
        {{--<h3 class="col-sm-12">Products Stats</h3>--}}
        {{--<div class="col-md-5">--}}
            {{--<div class="form-group">--}}
                {{--<label for="customerSelect">Select Product</label>--}}
                {{--<select class="js-select-options form-control" id="productSelect">--}}
                    {{--<option value="154">Menu 02</option>--}}
                    {{--<option value="153">tomato salad</option>--}}
                    {{--<option value="152">Fruit salad</option>--}}
                    {{--<option value="151">Potato Salad</option>--}}
                    {{--<option value="150">Milk coffee</option>--}}
                    {{--<option value="149">Turkich coffee</option>--}}
                    {{--<option value="148">Black coffee</option>--}}
                    {{--<option value="147">Salad</option>--}}
                    {{--<option value="146">Menu 01</option>--}}
                    {{--<option value="145">Orange juice</option>--}}
                    {{--<option value="144">Bana juice</option>--}}
                    {{--<option value="143">Poire juice</option>--}}
                    {{--<option value="142">spaghetti</option>--}}
                    {{--<option value="141">cheese burger</option>--}}
                    {{--<option value="140">Hamburger</option>--}}
                    {{--<option value="155">cola</option>--}}
                {{--</select>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-5">--}}
            {{--<div class="form-group">--}}
                {{--<label>Select Range</label>--}}
                {{--<div class="input-group margin-bottom-sm">--}}
                    {{--<span class="input-group-addon RangePicker"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>--}}
                    {{--<input class="form-control" id="ProductRange" type="text" name="daterangeP" />--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-2">--}}
            {{--<button class="cancelBtn btn btn-picker" type="button" onclick="getProductReport()">Get Report</button>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="row rangeStat" style="margin-top:50px;">--}}
        {{--<h3 class="col-sm-12">Register Stats</h3>--}}
        {{--<div class="col-md-5">--}}
            {{--<div class="form-group">--}}
                {{--<label for="customerSelect">Select Store</label>--}}
                {{--<select class="js-select-options form-control" id="StoresSelect">--}}
                    {{--<option value="3">zar POS store</option>--}}
                {{--</select>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-5">--}}
            {{--<div class="form-group">--}}
                {{--<label>Select Range</label>--}}
                {{--<div class="input-group margin-bottom-sm">--}}
                    {{--<span class="input-group-addon RangePicker"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>--}}
                    {{--<input class="form-control" id="RegisterRange" type="text" name="daterangeR" />--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-2">--}}
            {{--<button class="cancelBtn btn btn-picker" type="button" onclick="getRegisterReport()">Get Report</button>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<!-- ********************************************* warehouses report ***************************************************** -->--}}
    {{--<div class="row rangeStat" style="margin-top:50px;margin-bottom: 100px;">--}}
        {{--<h3 class="col-sm-12">Stock stats</h3>--}}
        {{--<div class="col-md-5">--}}
            {{--<div class="form-group">--}}
                {{--<label for="customerSelect">Select Store</label>--}}
                {{--<select class="js-select-options form-control" id="StockSelect">--}}
                    {{--<option value="S3">zar POS store</option>--}}
                    {{--<option value="W1">warehouse1</option>--}}
                {{--</select>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-5">--}}
            {{--<div class="form-group">--}}
                {{--<label></label>--}}
                {{--<div class="input-group margin-bottom-sm">--}}
                    {{--<span class="input-group-addon RangePicker"><i class="fa fa-calendar fa-fw" aria-hidden="true"></i></span>--}}
                    {{--<input class="form-control" id="" type="text" name="" disabled />--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-2">--}}
            {{--<button class="cancelBtn btn btn-picker" type="button" onclick="getStockReport()">Get Report</button>--}}
        {{--</div>--}}
    {{--</div>--}}

</div>
<!--[ footer ] -->
<div id="footer" style="background-color: #8297A8;width: 100%;">
    <div class="container">
        <p class="footer-block" style="margin: 20px 0;color:#fff;">POS - point of sale Dar Elweb.</p>
    </div>
</div>

<script>
    /******* Range date picker *******/
    $(function() {
        $('input[name="daterange"]').daterangepicker();
        $('input[name="daterangeP"]').daterangepicker();
        $('input[name="daterangeR"]').daterangepicker();
        var d = new Date().getFullYear();
        $('#ProductRange').val('01/01/'+d+' - 12/31/'+d);
        $('#CustomerRange').val('01/01/'+d+' - 12/31/'+d);
        $('#RegisterRange').val('01/01/'+d+' - 12/31/'+d);

    });
    /************************ Chart Data *************************/
    var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
    var lineChartData = {
        labels : ["January","February","March","April","May","June","July","August","September","October","November","December"],
        datasets : [
            {
                label: "Expences",
                backgroundColor: "rgba(255,99,132,0.2)",
                borderColor: "#FE9375",
                pointBackgroundColor: "#FE9375",
                pointBorderColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointHoverBorderColor: "#FE9375",
                data: [,,,,,,,,,,,]
            },
            {
                label: "Revenue",
                backgroundColor : "#2AC4C0",
                borderColor : "#26a5a2",
                pointBackgroundColor : "#2AC4C0",
                pointBorderColor : "#fff",
                pointHoverBackgroundColor : "#fff",
                pointHoverBorderColor : "#fff",
                data : [0,0,0,0,0,0,1100,0,0,0,0,0]
            }
        ]
    }
    window.onload = function(){

        // Chart.defaults.global.gridLines.display = false;

        var ctx = document.getElementById("canvas").getContext("2d");
        window.myLine = new Chart(ctx, {
            type: 'line',
            data: lineChartData,
            options: {
                scales : {
                    xAxes : [ {
                        gridLines : {
                            display : false
                        }
                    } ],
                    yAxes : [ {
                        gridLines : {
                            display : true
                        }
                    } ]
                },
                scaleFontSize: 9,
                tooltipFillColor: "rgba(0, 0, 0, 0.71)",
                tooltipFontSize: 10,
                responsive: true
            }});

        /********************* pie **********************/


        var pieData =  {
            labels: [
                "Menu 02",
                "tomato salad",
                "Salad",
                "Bana juice",
                "Potato Salad"
            ],
            datasets: [
                {
                    data: [17, 13, 12, 9, 9],
                    backgroundColor: [
                        "#F3565D",
                        "#FC9D9B",
                        "#FACDAE",
                        "#9FC2C4",
                        "#8297A8"
                    ],
                    hoverBackgroundColor: [
                        "#3e5367",
                        "#95a5a6",
                        "#f5fbfc",
                        "#459eda",
                        "#2dc6a8"
                    ],
                    hoverBorderWidth: [5,5,5,5,5]
                }
            ]
        };

        Chart.defaults.global.legend.display = false;

        var ctx2 = document.getElementById("chart-area2").getContext("2d");
        window.myPie = new Chart(ctx2,{
            type: 'doughnut',
            data: pieData});



        $('.count').each(function (index) {
            var size = $(this).text().split(".")[1] ? $(this).text().split(".")[1].length : 0;
            $(this).prop('count',0).animate({
                Counter: $(this).text()
            }, {
                duration: 2000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(parseFloat(now).toFixed(size));
                }
            });
        });


    }


    /********************************** Get repports functions ************************************/

    function getCustomerReport()
    {
        var client_id = $('#customerSelect').find('option:selected').val();
        var Range = $('#CustomerRange').val();
        var start = Range.slice(6,10)+'-'+Range.slice(0,2)+'-'+Range.slice(3,5);
        var end = Range.slice(19,23)+'-'+Range.slice(13,15)+'-'+Range.slice(16,18);
        // ajax delete data to database
        $.ajax({
            url : "http://www.dar-elweb.com/demos/zarest/reports/getCustomerReport/",
            type: "POST",
            data: {client_id: client_id, start: start, end: end},
            success: function(data)
            {
                $('#statsSection').html(data);
                $('#stats').modal('show');
                var table = $('#Table').DataTable( {
                    dom: 'T<"clear">lfrtip',
                    tableTools: {
                        'bProcessing'    : true
                    }
                });
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert("error");
            }
        });

    }

    function getProductReport()
    {
        var product_id = $('#productSelect').find('option:selected').val();
        var Range = $('#ProductRange').val();
        var start = Range.slice(6,10)+'-'+Range.slice(0,2)+'-'+Range.slice(3,5);
        var end = Range.slice(19,23)+'-'+Range.slice(13,15)+'-'+Range.slice(16,18);
        // ajax set data to database
        $.ajax({
            url : "{{url("reports/getProductReport/")}}",
            type: "GET",
            data: {product_id: product_id, start: start, end: end},
            success: function(data)
            {
                $('#statsSection').html(data);
                $('#stats').modal('show');
                var table = $('#Table').DataTable( {
                    dom: 'T<"clear">lfrtip',
                    tableTools: {
                        'bProcessing'    : true
                    }
                });
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert("error");
            }
        });
    }

    function getRegisterReport()
    {
        var store_id = $('#StoresSelect').find('option:selected').val();
        var Range = $('#RegisterRange').val();
        var start = Range.slice(6,10)+'-'+Range.slice(0,2)+'-'+Range.slice(3,5);
        var end = Range.slice(19,23)+'-'+Range.slice(13,15)+'-'+Range.slice(16,18);
        // ajax set data to database
        $.ajax({
            url : "http://www.dar-elweb.com/demos/zarest/reports/getRegisterReport/",
            type: "POST",
            data: {store_id: store_id, start: start, end: end},
            success: function(data)
            {
                $('#statsSection').html(data);
                $('#stats').modal('show');
                var table = $('#Table').DataTable( {
                    dom: 'T<"clear">lfrtip',
                    tableTools: {
                        'bProcessing'    : true
                    }
                });
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert("error");
            }
        });
    }

    function getStockReport()
    {
        var stock_id = $('#StockSelect').find('option:selected').val();
        // ajax set data to database
        $.ajax({
            url : "http://www.dar-elweb.com/demos/zarest/reports/getStockReport/",
            type: "POST",
            data: {stock_id: stock_id},
            success: function(data)
            {
                $('#statsSection').html(data);
                $('#stats').modal('show');
                var table = $('#Table').DataTable( {
                    dom: 'T<"clear">lfrtip',
                    tableTools: {
                        'bProcessing'    : true
                    }
                });
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert("error");
            }
        });
    }

    function getyearstats(direction) {
        var currentyear = parseInt($('.statYear').text());
        var year = direction === 'next' ? currentyear-1 : currentyear+1;

        $.ajax({
            url : "http://www.dar-elweb.com/demos/zarest/reports/getyearstats/"+year,
            type: "POST",
            success: function(data)
            {
                $('#statyears').html(data);
                $('.statYear').text(year);
                $('[data-toggle="tooltip"]').tooltip();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert("error");
            }
        });
    }

    function RegisterDetails(id) {
        $.ajax({
            url : "http://www.dar-elweb.com/demos/zarest/reports/RegisterDetails/"+id,
            type: "POST",
            success: function(data)
            {
                $('#RegisterDetails').html(data);
                $('#stats').modal('hide');
                $('#RegisterDetail').modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert("error");
            }
        });
    }

    function CloseRegisterDetails(){
        $('#RegisterDetail').modal('hide');
        $('#stats').modal('show');
    }

    function delete_register(id){
        swal({   title: 'Are you sure ?',
                text: 'You will not be able to recover this Data later!',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false },
            function(){
                $.ajax({
                    url : "http://www.dar-elweb.com/demos/zarest/reports/delete_register/"+id,
                    type: "POST",
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert("error");
                    }
                });
                $('#stats').modal('hide');
                swal('Deleted!', 'the data has been deleted.', "success"); });
    }


</script>

<!-- Modal stats -->
<div class="modal fade" id="stats" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" id="statsModal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="stats">Stats</h4>
            </div>
            <div class="modal-body" id="modal-body">
                <div id="statsSection">
                    <!-- stats goes here -->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default hiddenpr" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- /.Modal -->

<!-- Modal register -->
<div class="modal fade" id="RegisterDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Register Details</h4>
            </div>
            <div class="modal-body">
                <div id="RegisterDetails">
                    <!-- close register detail goes here -->
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" onclick="CloseRegisterDetails()" class="btn btn-orange col-md-12 flat-box-btn">Return</a>
            </div>
        </div>
    </div>
</div>
<!-- /.Modal -->




<!-- slim scroll script -->
<script type="text/javascript" src="http://www.dar-elweb.com/demos/zarest/assets/js/jquery.slimscroll.min.js"></script>
<!-- waves material design effect -->
<script type="text/javascript" src="http://www.dar-elweb.com/demos/zarest/assets/js/waves.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script type="text/javascript" src="http://www.dar-elweb.com/demos/zarest/assets/js/bootstrap.min.js"></script>
<!-- keyboard widget dependencies -->
<script type="text/javascript" src="http://www.dar-elweb.com/demos/zarest/assets/js/jquery.keyboard.js"></script>
<script type="text/javascript" src="http://www.dar-elweb.com/demos/zarest/assets/js/jquery.keyboard.extension-all.js"></script>
<script type="text/javascript" src="http://www.dar-elweb.com/demos/zarest/assets/js/jquery.keyboard.extension-extender.js"></script>
<script type="text/javascript" src="http://www.dar-elweb.com/demos/zarest/assets/js/jquery.keyboard.extension-typing.js"></script>
<script type="text/javascript" src="http://www.dar-elweb.com/demos/zarest/assets/js/jquery.mousewheel.js"></script>
<!-- select2 plugin script -->
<script type="text/javascript" src="http://www.dar-elweb.com/demos/zarest/assets/js/select2.min.js"></script>
<!-- dalatable scripts -->
<script src="http://www.dar-elweb.com/demos/zarest/assets/datatables/js/jquery.dataTables.min.js"></script>
<script src="http://www.dar-elweb.com/demos/zarest/assets/datatables/js/dataTables.bootstrap.js"></script>
<!-- summernote js -->
<script src="http://www.dar-elweb.com/demos/zarest/assets/js/summernote.js"></script>
<!-- chart.js script -->
<script src="http://www.dar-elweb.com/demos/zarest/assets/js/Chart.js"></script>
<!-- moment JS -->
<script type="text/javascript" src="http://www.dar-elweb.com/demos/zarest/assets/js/moment.min.js"></script>
<!-- Include Date Range Picker -->
<script type="text/javascript" src="http://www.dar-elweb.com/demos/zarest/assets/js/daterangepicker.js"></script>
<!-- Sweet Alert swal -->
<script src="http://www.dar-elweb.com/demos/zarest/assets/js/sweetalert.min.js"></script>
<!-- datepicker script -->
<script src="http://www.dar-elweb.com/demos/zarest/assets/js/bootstrap-datepicker.min.js"></script>
<!-- creditCardValidator script -->
<script src="http://www.dar-elweb.com/demos/zarest/assets/js/jquery.creditCardValidator.js"></script>
<!-- creditCardValidator script -->
<script src="http://www.dar-elweb.com/demos/zarest/assets/js/credit-card-scanner.js"></script>
<script src="http://www.dar-elweb.com/demos/zarest/assets/js/jquery.redirect.js"></script>
<!-- ajax form -->
<script src="http://www.dar-elweb.com/demos/zarest/assets/js/jquery.form.min.js"></script>
<!-- custom script -->
<script src="http://www.dar-elweb.com/demos/zarest/assets/js/app.js"></script>
</body>
</html>
