<!-- select2 -->
<div @include('crud::inc.field_wrapper_attributes') >
    <label>{!! $field['label'] !!}</label>
    @include('crud::inc.field_translatable_icon')
    <select
            name="{{ $field['name'] }}"
            style="width: 100%"
            @include('crud::inc.field_attributes', ['default_class' =>  'form-control select2_field'])>
        <option value="">-</option>
        @if (isset($field['model']))
            @foreach ($field['model2']::all() as $connected_entity_entry)
                <option value="{{ $connected_entity_entry->getKey() }}">{{ $connected_entity_entry->{$field['attribute2']} }}</option>

            @endforeach
        @endif
    </select>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
</div>

{{-- ########################################## --}}
{{-- Extra CSS and JS for this particular field --}}
{{-- If a field type is shown multiple times on a form, the CSS and JS will only be loaded once --}}
@if ($crud->checkIfFieldIsFirstOfItsType($field, $fields))

    {{-- FIELD CSS - will be loaded in the after_styles section --}}
    @push('crud_fields_styles')
    <!-- include select2 css-->
    <link href="{{ asset('vendor/adminlte/bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
          rel="stylesheet" type="text/css"/>
    @endpush

    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')
    <!-- include select2 js-->
    <script src="{{ asset('vendor/adminlte/bower_components/select2/dist/js/select2.min.js') }}"></script>
    <script>
        jQuery(document).ready(function ($) {
            // trigger select2 for each untriggered select2 box
            $('.select2_field').each(function (i, obj) {
                if (!$(obj).hasClass("select2-hidden-accessible")) {
                    $(obj).select2({
                        theme: "bootstrap"
                    }).on("change", function () {
                        var tr = '';
                        var product_id = $(this).val();
                        var product_name = $(this).find('option:selected').text();
                        tr = '<tr class="product_id">' + '<td>' +1+ '</td>' +
                        '<td id="product"><input type="hidden" name="product_id[]" class="product_id" value="'+product_id+'">' + product_name + '</td>' +
                        '<td>' + '<input class="form-control price" value="0"  name="price[]">' + '</td> ' +
                        '<td>' + '<input class="form-control qty" value="1"  name="qty[]"> ' + '</td> ' +

                        '<td>' +
                        '<input class="form-control discount"  name="discount[]">' +
                        '</td>' +
                        '<td>' +
                        '<input class="form-control subtotal" name="subtotal[]" readonly>' +
                        '</td>' + '<td> ' + '<button class="btn btn-danger btn-xs remove-product" type="button">' +
                        '<i class="fa fa-times" aria-hidden="true"></i>Remove' +
                        '</button>' + '</td> ' +
                        '</tr>';

                        $('#purchase').append(tr);
                    });
                }
            });

        });
    </script>
    @endpush

@endif
{{-- End of Extra CSS and JS --}}
{{-- ########################################## --}}